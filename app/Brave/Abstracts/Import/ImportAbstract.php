<?php

namespace Brave\Abstracts\Import;

use \Brave\Contracts\Import\ImportContract;

abstract class ImportAbstract implements ImportContract {

	protected $data = [];

	/**
	 * @param $data
	 */
	function __construct($data) {
		$this->data = $data;
	}

	/**
	 * @return bool|mixed
	 * @throws \Exception
	 */
	public function run() {

		try {

			if(empty($this->data)) {
				throw new \Exception('Data must be set for '.static::class.' to work.');
			}

			// run the actual import method.
			return $this->_run();
		}
		catch (\Exception $e) {

			// we got an error, LOG IT OMG
			var_dump($e->getMessage());
			\Log::error($e->getMessage());
			return false;
		}
	}

}
<?php


namespace Brave\Contracts\Import;

/**
 * Interface ImportContract
 *
 * @package Brave\Contracts
 */
interface ImportContract {

	/**
	 * @return bool
	 */
	public function run();

}
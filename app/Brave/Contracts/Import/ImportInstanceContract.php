<?php


namespace Brave\Contracts\Import;

/**
 * Interface ImportContract
 *
 * @package Brave\Contracts
 */
interface ImportInstanceContract {

	/**
	 * @return bool
	 */
	public function _run();

}
<?php

namespace Brave\Eve\Security;

/**
 * Class Security
 *
 * @package Brave\Eve\Security
 */
Class Security {

	/**
	 * @var array
	 */
	protected static $colors = array(
		'1.0' => '2FEFEF',
		'0.9' => '48F0C0',
		'0.8' => '00EF47',
		'0.7' => '00F000',
		'0.6' => '8FEF2F',
		'0.5' => 'EFEF00',
		'0.4' => 'D77700',
		'0.3' => 'F06000',
		'0.2' => 'F04800',
		'0.1' => 'D73000',
		'0.0' => 'F00000',
		'-0.1' => 'F00000',
		'-0.2' => 'F00000',
		'-0.3' => 'F00000',
		'-0.4' => 'F00000',
		'-0.5' => 'F00000',
		'-0.6' => 'F00000',
		'-0.7' => 'F00000',
		'-0.8' => 'F00000',
		'-0.9' => 'F00000',
		'-1.0' => 'F00000',
	);

	/**
	 * @param $rating
	 * @return null
	 */
	static public function getColorCode($rating)
	{
		if(is_float($rating))
		{
			return self::$colors[ (string) $rating ];
		}
		else
		{
			return null;
		}
	}
}
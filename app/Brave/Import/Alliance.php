<?php

namespace Brave\Import;

use Brave\Abstracts\Import\ImportAbstract;
use Brave\Contracts\Import\ImportInstanceContract;

/**
 * Class Alliance
 *
 * @package Brave\Import
 */
class Alliance extends ImportAbstract implements ImportInstanceContract {

	/**
	 * @return bool
	 */
	public function _run() {

		// get alliance info
		$alliance = \Alliance::firstOrNew(['id' => $this->data->allianceID]);

		$alliance->name = $this->data->name;
		$alliance->shortName = $this->data->shortName;
		$alliance->executorCorpID = $this->data->executorCorpID;
		$alliance->memberCount = $this->data->memberCount;
		$alliance->startDate = $this->data->startDate;

		$alliance->save();

		return $alliance;
	}
}

<?php

namespace Brave\Import;

use Brave\Abstracts\Import\ImportAbstract;
use Brave\Contracts\Import\ImportInstanceContract;

/**
 * Class Corporation
 *
 * @package Brave\Import
 */
class Corporation extends ImportAbstract implements ImportInstanceContract {

	/**
	 * @return bool
	 */
	public function _run() {

		$corp = \Corporation::firstOrNew([
			'id' => $this->data->corporationID,
		]);

		$corp->name = $this->data->corporationName;
		$corp->shortName = $this->data->ticker;
		$corp->ceoID = $this->data->ceoID;
		$corp->ceoName = $this->data->ceoName;
		$corp->stationID = $this->data->stationID;
		$corp->stationName = $this->data->stationName;
		$corp->description = $this->data->description;
		$corp->url = $this->data->url;
		$corp->allianceID = $this->data->allianceID;
		$corp->factionID = $this->data->factionID;
		$corp->taxRate = $this->data->taxRate;
		$corp->shares = $this->data->shares;

		$corp->save();

		return $corp;
	}
}

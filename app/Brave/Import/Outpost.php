<?php

namespace Brave\Import;

use Brave\Abstracts\Import\ImportAbstract;
use Brave\Contracts\Import\ImportInstanceContract;

/**
 * Class Outpost
 *
 * @package Brave\Import
 */
class Outpost extends ImportAbstract implements ImportInstanceContract {

	/**
	 * @return bool
	 */
	public function _run() {

		$planet_name = explode(' - ', $this->data->stationName)[0];

		$planet = \MapItem::where('itemName', '=', $planet_name)->first();

		// get alliance info
		$station = \Station::firstOrNew(['id' => $this->data->stationID]);

		$station->itemID = $planet->itemID;
		$station->name = $this->data->stationName;
		$station->type = $this->data->stationTypeID;
		$station->solarSystemID = $this->data->solarSystemID;
		$station->corporationID = $this->data->corporationID;
		$station->corporationName = $this->data->corporationName;
		$station->corporationID = $this->data->corporationID;

		$station->save();

		return $station;
	}
}

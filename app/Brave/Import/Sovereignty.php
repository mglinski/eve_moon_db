<?php

namespace Brave\Import;

use Brave\Abstracts\Import\ImportAbstract;
use Brave\Contracts\Import\ImportInstanceContract;

/**
 * Class Sovereignty
 *
 * @package Brave\Import
 */
class Sovereignty extends ImportAbstract implements ImportInstanceContract {

	/**
	 * @return bool
	 */
	public function _run() {

		// get alliance info
		$sovereignty = \Sovereignty::firstOrNew(['id' => $this->data->solarSystemID]);

		$sovereignty->solarSystemName = $this->data->solarSystemName;
		$sovereignty->allianceID = $this->data->allianceID;
		$sovereignty->factionID = $this->data->factionID;
		$sovereignty->corporationID = $this->data->corporationID;

		$sovereignty->save();


		return $sovereignty;
	}
}

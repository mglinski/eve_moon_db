<?php
/**
 * AddUser.php
 * Created by: matthewglinski
 * Date: 12/30/14 4:11 PM
 */

namespace Brave\Import;

use Brave\Abstracts\Import\ImportAbstract;
use Brave\Contracts\Import\ImportInstanceContract;

/**
 * Class User
 *
 * @package Brave\Import
 */
class User extends ImportAbstract implements ImportInstanceContract {

	/**
	 * @return bool
	 */
	public function _run() {

		$user = \User::create([
			'username' => $this->data->name,
			'password' => \Hash::make($this->data->password),
			'email' => $this->data->email,
			'status' => '1',
			'permission' => (int)$this->data->permission
		]);

		return $user;
	}

}
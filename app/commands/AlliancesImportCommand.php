<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Eve\Eve;
use Eve\Corporation as EveCorporation;

use Brave\Import\Alliance as AllianceImport;
use Brave\Import\Corporation as CorporationImport;

class AlliancesImportCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'updateAlliances';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Re-import Alliance List!';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$this->info('Downloading Alliance List....');
		$alliances = Eve::AllianceList();
		$this->info('Done.');

		$this->info('Importing Alliance List....');
		foreach ($alliances->alliances as $id => $alliance_data) {

			$this->info('Alliance ('.$alliance_data->name.') Importing...');

			// import alliance data!
			(new AllianceImport($alliance_data))->run();

			// load in corps
			foreach ($alliance_data->memberCorporations as $corp) {
				$corp_data = EveCorporation::CorporationSheetPublic($corp->corporationID);
				$this->info('  Corporation ('.$corp_data->corporationName.') Importing...');

				// import corp data
				(new CorporationImport($corp_data))->run();
			}

			// print newline
			$this->info('');
		}

		$this->info('Import Complete!');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
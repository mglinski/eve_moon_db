<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DotlanMapsImportCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'updateMaps';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Re-import Dotlan maps!';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$this->info('Importing Dotlan Maps....');

		$regions = RegionItem::where('regionID', '<', '11000001')->whereNotIn('regionID', RegionItem::$joveRegions)->orderBy('regionName', 'asc')->get();
		foreach($regions as $region)
		{
			$name = str_replace(' ', '_', $region->regionName);

			$this->info($name);

			$request = Requests::get('http://evemaps.dotlan.net/svg/'.$name.'.svg');
			if(isset($request->body) and $request->body != '')
			{
				$lol = str_replace('http://evemaps.dotlan.net/system/', URL::to('map/system').'/', $request->body);
				$lol = str_replace('http://evemaps.dotlan.net/map/', URL::to('map/region').'/', $lol);
				$lol = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $lol);
				file_put_contents('public/maps/'.$name.'.svg', $lol);
			}
		}
		$this->info('Done.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Eve\Map;
use Eve\Corporation as EveCorporation;

use Brave\Import\Sovereignty as SovereigntyImport;

class MassUpdateCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'massUpdate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Re-Import Everything except alliances!';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$this->info('Starting Mass Update....');

		$this->call('updateMaps');
		$this->call('updateSovereignty');
		$this->call('updateOutpost');

		if($this->option('alliances')) {
			$this->call('updateAlliances');
		}

		$this->info('Mass Import Complete!');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(

			array('alliances', 'a', InputOption::VALUE_NONE, 'Also import alliances, THIS TAKES A LONG TIME I WARNED YOU')

		);
	}

}
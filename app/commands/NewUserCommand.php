<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Brave\Import\User as UserImport;

class NewUserCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'newUser';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Add a user to the app';

	/**
	 * Create a new command instance.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		// get the desired username
		$name = $this->ask('What is the username?');

		// check that the username is not in use
		$names = User::where('username', '=', $name)->get();
		if (count($names) !== 0) {
			$this->error('A user with this username already exists. Please try again.');
			return;
		}

		// get user email address
		$email = $this->ask('What is the email address?');

		// check that the email address is not in use
		$emails = User::where('email', '=', $email)->get();
		if (count($emails) !== 0) {
			$this->error('A user with this email already exists. Please try again.');
			return;
		}

		// does this user have permission to edit data?
		if ($this->confirm('Does user have Edit Permissions [yes|no]')) {
			$permission = true;
		}
		else {
			$permission = false;
		}

		// get and validate the desired password
		$password = $this->secret('What is the password?');
		$password2 = $this->secret('Please confirm the password (type again):');
		if ($password !== $password2) {
			$this->error('Entered Passwords do not match. Please try again.');
			return;
		}

		// do final validation
		$this->info('You are about to create this user:');
		$this->info('Username: '.$name);
		$this->info('Email: '.$email);
		$this->info('Password: '.$password);
		$this->info('Edit Permissions: '.($permission ? 'Yes' : 'No') );

		// and confirmation
		if ($this->confirm('Do you wish to continue? [yes|no]'))
		{
			// make the user !
			$data = [
				'name' => $name,
				'password' => $password,
				'email' => $email,
				'permission' => (int)$permission
			];

			// import user data
			(new UserImport((object)$data))->run();

			$this->info('User Created!');
			return;
		}

		// no final confirmation, do nothing.
		$this->info('User NOT Created. Aborting.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
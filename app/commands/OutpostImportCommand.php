<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Eve\Eve;

use Brave\Import\Outpost as OutpostImport;

class OutpostImportCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'updateOutpost';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Re-import Outpost List!';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$this->info('Downloading Outpost List....');
		$outposts = Eve::ConquerableStationList();
		$this->info('Done.');

		$this->info('Importing Outpost List....');
		foreach ($outposts->outposts as $id => $outpost_data) {

			//$this->info('  SolarSystem ('.$sovereignty_data->solarSystemName.') Importing Data...');

			// import alliance data!
			(new OutpostImport($outpost_data))->run();
		}

		$this->info('Import Complete!');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
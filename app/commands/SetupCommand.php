<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Eve\Map;
use Eve\Corporation as EveCorporation;

use Brave\Import\Sovereignty as SovereigntyImport;

class SetupCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'app:setup';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Do app things on initinal install!';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$this->info('Setting up the App....');

		$this->call('key:generate');
		$this->call('migrate:install');
		$this->call('migrate');
		$this->call('optimize');

		$this->call('updateMaps');
		$this->call('updateSovereignty');
		$this->call('updateOutposts');
		$this->call('updateAlliances');

		$this->info('Setup Complete!');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
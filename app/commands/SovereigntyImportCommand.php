<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Eve\Map;

use Brave\Import\Sovereignty as SovereigntyImport;

class SovereigntyImportCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'updateSovereignty';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Re-import Sovereignty List!';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$this->info('Downloading Sovereignty List....');
		$sovereignty = Map::Sovereignty();
		$this->info('Done.');

		$this->info('Importing Sovereignty List....');
		foreach ($sovereignty->solarSystems as $id => $sovereignty_data) {

			//$this->info('  SolarSystem ('.$sovereignty_data->solarSystemName.') Importing Data...');

			// import alliance data!
			(new SovereigntyImport($sovereignty_data))->run();

		}

		$this->info('Import Complete!');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
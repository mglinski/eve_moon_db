<?php

class CelestialController extends BaseController
{
	const LAYOUT = 'layouts.home';

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function universeView()
	{
		$regions = RegionItem::where('regionID', '<', '11000001')->whereNotIn('regionID', RegionItem::$joveRegions)->orderBy('regionName', 'asc')->get();

		// recent updates
		$posUpdates = Pos::getRecentUpdates();
		$pocoUpdates = Poco::getRecentUpdates();

		$this->layout = self::LAYOUT;
		$view = View::make(self::LAYOUT)
		            ->nest('navigation', 'navigation')
		            ->nest('footer', 'parts/footer')
		            ->nest('page_content', 'celestial/home',
		                   array(
			                   'regions' => $regions,
			                   'posUpdates' => $posUpdates,
			                   'pocoUpdates' => $pocoUpdates
		                   )
			);
		return $view;
	}

	public function universeSystemView($id)
	{
		// resolve names to ids
		if(!is_numeric($id))
		{
			// http://evemaps.dotlan.net/system/
			$ssn = SolarSystemItem::where('solarSystemName', '=', urldecode($id))->get();
			$solar = @$ssn[0];
			if(!empty($solar))
			{
				$id = $solar->solarSystemID;
			}
			else
			{
				App::abort(404);
			}
		}

		$celestials = MapItem::where('solarSystemID', '=', $id)->whereIn('groupID', array(7, 8))->where('security', '<', 1.0)->orderBy('itemID', 'asc')->get();
		$ihubs = Ihub::getBySystemID($id);
		$stations = Station::getBySystemID($id);

		if($celestials == false)
		{
			return Redirect::route('home')
			       ->with('flash_error', 'System does not exist');
		}

		$system = SolarSystemItem::find($id);
		$region = RegionItem::find($system->regionID);

		// recent updates
		$posUpdates = Pos::getRecentUpdatesBySolarSystemID($id);
		$pocoUpdates = Poco::getRecentUpdatesBySolarSystemID($id);

		$this->layout = self::LAYOUT;
		$view = View::make(self::LAYOUT)
	        ->nest('navigation', 'navigation')
	        ->nest('footer', 'parts/footer')
	        ->nest('page_content', 'celestial/system', array(
				'system' => $system,
				'region' => $region,
				'celestials' => $celestials,
				'ihubs' => $ihubs,
				'stations' => $stations,
				'posUpdates' => $posUpdates,
				'pocoUpdates' => $pocoUpdates,
			)
		);

		return $view;
	}

	public function universeRegionView($id)
	{
		// resolve names to ids
		if(!is_numeric($id))
		{
			// http://evemaps.dotlan.net/system/
			$ssn = RegionItem::where('regionName', '=', urldecode($id))->get();
			$solar = @$ssn[0];
			if(!empty($solar))
			{
				$id = $solar->regionID;
			}
			else
			{
				App::abort(404);
			}
		}

		$systems = SolarSystemItem::where('regionID', '=', $id)->orderBy('solarSystemName', 'asc')->get();

		if($systems == false)
		{
			return Redirect::route('home')
		       ->with('flash_error', 'Region does not exist');
		}
		$region = RegionItem::find($id);

		$pos_num = MapItem::where('regionID', '=', $id)->where('groupID', '=', 8)->count();
		$pos_mapped_num = Pos::getCountOfMappedMoonsInRegionByID($id);

		$poco_num = MapItem::where('regionID', '=', $id)->where('groupID', '=', 7)->count();
		$poco_mapped_num = Poco::getCountOfMappedPlanetsInRegionByID($id);

		// get mats in system
		$mats = array();
		foreach(Mat::$matTypes as $mat_id => $name)
		{
			if($mat_id >= 16637)
			{
				$mats[$name] = Mat::getMaterialCountByRegion($id, array($mat_id));
			}
		}

		// get poco shit
		$posUpdates = Pos::getRecentUpdatesByRegionID($id);
		$pocoUpdates = Poco::getRecentUpdatesByRegionID($id);

		$this->layout = self::LAYOUT;
		$view = View::make(self::LAYOUT)
	        ->nest('navigation', 'navigation')
	        ->nest('footer', 'parts/footer')
	        ->nest('page_content', 'celestial/region',
	        array(
				'pos_num' => $pos_num,
				'pos_mapped_num' => $pos_mapped_num,
				'poco_num' => $poco_num,
				'poco_mapped_num' => $poco_mapped_num,
				'region' => $region,
				'systems' => $systems,
				'mats' => $mats,
				'posUpdates' => $posUpdates,
				'pocoUpdates' => $pocoUpdates
			)
		);

		return $view;
	}

	// -----------------------------------------------



	public function universeRegionExport($id)
	{
		// resolve names to ids
		if(!is_numeric($id))
		{
			// http://evemaps.dotlan.net/system/
			$ssn = RegionItem::where('regionName', '=', urldecode($id))->get();
			$solar = @$ssn[0];
			if(!empty($solar))
			{
				$id = $solar->regionID;
			}
			else
			{
				App::abort(404);
			}
		}

		$celestials = MapItem::where('regionID', '=', $id)->whereIn('groupID', array(7, 8))->where('security', '<', 1.0)->orderBy('itemID', 'asc')->get();

		if($celestials == false)
		{
			return Redirect::route('home')
				->with('flash_error', 'System does not exist');
		}

		$this->layout = self::LAYOUT;
		$view = View::make('celestial/export/system', array(
			'celestials' => $celestials,
		));

		return $view;
	}



	public function universeSystemExport($id)
	{
		// resolve names to ids
		if(!is_numeric($id))
		{
			// http://evemaps.dotlan.net/system/
			$ssn = SolarSystemItem::where('solarSystemName', '=', urldecode($id))->get();
			$solar = @$ssn[0];
			if(!empty($solar))
			{
				$id = $solar->solarSystemID;
			}
			else
			{
				App::abort(404);
			}
		}

		$celestials = MapItem::where('solarSystemID', '=', $id)->whereIn('groupID', array(7, 8))->where('security', '<', 1.0)->orderBy('itemID', 'asc')->get();

		if($celestials == false)
		{
			return Redirect::route('home')
				->with('flash_error', 'System does not exist');
		}

		$this->layout = self::LAYOUT;
		$view = View::make('celestial/export/system', array(
			'celestials' => $celestials,
		));

		return $view;
	}
}
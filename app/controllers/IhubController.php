<?php

class IhubController extends BaseController
{
	const LAYOUT = 'layouts.home';

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function insertView($id)
	{
		$solarSystem = SolarSystemItem::find($id);

		// moon does not exist
		if($solarSystem == false)
		{
			return Redirect::route('home')
				->with('flash_error', 'Solar System does not exist');
		}

		$planets = array();
		$map_planets = MapItem::where('solarSystemID', '=', $id)->where('groupID', '=', Ihub::$groupID)->get();
		foreach($map_planets as $planet)
		{
			$planets[$planet->itemID] = $planet->itemName;
		}

		$this->layout = self::LAYOUT;
		$view = View::make(self::LAYOUT)
	        ->nest('navigation', 'navigation')
	        ->nest('footer', 'parts/footer')
	        ->nest('page_content', 'celestial/ihub/add', array(
				'solarSystem' => $solarSystem,
				'planets' => $planets
			));
		return $view;
	}

	public function editView($id)
	{
		$ihub = Ihub::getByID($id);

		if($ihub == false)
		{
			return Redirect::route('home')
		       ->with('flash_error', 'IHUB does not exist');
		}

		$solarSystem = MapItem::find($ihub->solarSystemID);

		$planets = array();
		$map_planets = MapItem::where('solarSystemID', '=', $ihub->solarSystemID)->where('groupID', '=', Ihub::$groupID)->get();
		foreach($map_planets as $planet)
		{
			$planets[$planet->itemID] = $planet->itemName;
		}

		$this->layout = self::LAYOUT;
		$view = View::make(self::LAYOUT)
	        ->nest('navigation', 'navigation')
	        ->nest('footer', 'parts/footer')
	        ->nest('page_content', 'celestial/ihub/edit',
	               array(
		               'solarSystem' => $solarSystem,
		               'planets' => $planets,
		               'ihub' => $ihub
	               )
		);
		return $view;
	}

	public function insertAction($id)
	{
		$rules = array(
			'planet' => 'required',
			'timer' => 'required',
			'status' => 'required',
			'owner' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails())
		{
			return Redirect::route('ihub_add', array($id))
		       ->with('flash_error', 'Please fill in all IHUB Information')
		       ->withInput();
		}
		else
		{
			// DO REGISTRATION
			$celestial = MapItem::find(Input::get('planet'));
			$system = MapItem::find($id);
			if($celestial !== false)
			{
				$owner = Input::get('owner');
				if (Input::get('corporationID') != '') {
					$owner = Input::get('corporationID');
				}

				Ihub::create(
					array(
						'itemID' => $celestial->itemID,
						'solarSystemID' => $system->itemID,
						'owner' => $owner,
						'timer' => Input::get('timer'),
						'status' => Input::get('status')
					)
				);

				return Redirect::route('system', array($celestial->solarSystemID))
			       ->with('flash_msg', 'New iHub on "'.$celestial->itemName.'" Successfully Added')
			       ->withInput();
			}
			else
			{
				return Redirect::route('home')
			       ->with('flash_error', 'This celestial does not exist in the Database')
			       ->withInput();
			}
		}
	}

	public function editAction($id)
	{
		$rules = array(
			'planet' => 'required',
			'timer' => 'required',
			'status' => 'required',
			'owner' => 'required',
		);

		$ihub = Ihub::getByID($id);

		if($ihub == false)
		{
			return Redirect::route('home')
		       ->with('flash_error', 'iHub does not exist')
		       ->withInput();
		}

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::route('ihub_edit', array($id))
		       ->with('flash_error', 'Please fill in all iHub Information')
		       ->withInput();
		}
		else
		{
			// DO REGISTRATION
			$celestial = MapItem::find(Input::get('planet'));
			$system = MapItem::find($ihub->solarSystemID);
			if($celestial !== false)
			{
				$owner = Input::get('owner');
				if (Input::get('corporationID') != '') {
					$owner = Input::get('corporationID');
				}

				$ihub->itemID = $celestial->itemID;
				$ihub->solarSystemID = $system->itemID;
				$ihub->owner = $owner;
				$ihub->timer = Input::get('timer');
				$ihub->status = Input::get('status');

				$ihub->save();

				return Redirect::route('system', array($celestial->solarSystemID))
				               ->with('flash_msg', 'iHub on "'.$celestial->itemName.'" Successfully Edited')
				               ->withInput();
			}
			else
			{
				return Redirect::route('home')
				               ->with('flash_error', 'This celestial does not exist in the Database')
				               ->withInput();
			}
		}
	}

	public function deleteAction($id)
	{
		$ihub = Ihub::getByID($id);

		if($ihub == false)
		{
			return Redirect::route('home')
		       ->with('flash_error', 'iHub does not exist')
		       ->withInput();
		}

		// delete that shit
		Ihub::find($id)->delete();

		return Redirect::route('system', array($ihub->solarSystemID))
	       ->with('flash_msg', 'iHub "'.$ihub->itemName.'" Was Deleted')
	       ->withInput();
	}

}
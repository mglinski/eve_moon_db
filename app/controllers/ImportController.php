<?php

class ImportController extends BaseController
{
	const LAYOUT = 'layouts.home';


	function __construct()
	{
		parent::__construct();
		set_time_limit(0);
	}

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function importTenerifisView()
	{
		$this->layout = self::LAYOUT;
		$view = View::make(self::LAYOUT)
		            ->nest('navigation', 'navigation')
		            ->nest('footer', 'parts/footer')
		            ->nest('page_content', 'import/tenerifis');

		return $view;
	}

	public function importTenerifisAction()
	{
		// DO REGISTRATION
		$import = Input::get('massImport');

		if($import !== false)
		{
			$final = array();
			$mat_ids = array_flip(Mat::$matTypes);

			$lines = explode("\n", $import);
			foreach($lines as $i => $line)
			{
				if(trim($line) == '')
				{
					continue;
				}

				$final[$i] = array(
					'name' => '',
					'mats' => array()
				);
				$sections = str_getcsv($line, "\t");
				foreach($sections as $x => $tab)
				{

					switch($x)
					{
						case 0:  // System
							$final[$i]['name'] = trim($tab);
							break;

						case 1: // Planet
							$final[$i]['name'] .= ' '.self::intToRoman(trim($tab));
							break;

						case 2: // Moon
							$final[$i]['name'] .= ' - Moon '.trim($tab);
							break;

						case 3:
						case 4:
						case 5:
						case 6:
							$mats = array();
							$name = trim($tab);

							/// make sure its important
							if(strtoupper($name) === 'EMPTY' OR strtoupper($name) === 'NONE')
							{
								$mats['name'] = 'Barren';
								$mats['abundance'] = '1';
								$final[$i]['mats'][] = $mats;
								continue 2;
							}

							if($name !== "" AND !isset($mat_ids[ucwords($name)]))
							{
								echo "Error Found on line ".($i+1)." of the spreadsheet: '".$name."' Is not a valid material name.";
								exit;
							}

							/// make sure its important
							if(strtoupper($name) != 'EMPTY' and $name !== "")
							{
								$mats['name'] = trim($tab);
								$mats['abundance'] = 1;

								$final[$i]['mats'][] = $mats;
							}
							else
							{
								if($x == 3)
								{
									$mats['name'] = 'Barren';
									$mats['abundance'] = '1';
									$final[$i]['mats'][] = $mats;
								}
							}
							break;
					}
				}
			}

			$i = 0;
			$pos_insert = [];
			$mat_insert = [];
			$date = date('Y-m-d H:i:s');


			foreach($final as $i => $moon)
			{
				//var_dump($moon);

				$data = MapItem::where('itemName', '=', $moon['name'])->where('groupID', '=', Pos::$groupID)->get();
				if(isset($data[0]) && isset($data[0]->itemID))
				{
					$map = $data[0];
					$pos = Pos::find($map->itemID);
					if(!empty($pos->itemID))
					{
						// Old POS exists, delete current materials
						Mat::where('itemID', '=', $map->itemID)->delete();
					}
					else
					{
						// No Pos Exists, Setup new base POS
						$pos_insert[$map->itemID] = array(
							'itemID' => $map->itemID,
							'type' => 0,
							'status' => 0,
							'password' => '',
							'owner' => '',
							'dscan' => '',
							'created_at' => $date,
							'updated_at' => $date
						);
					}

					// Insert Mats
					foreach($moon['mats'] as $mat)
					{
						$mat_insert[$map->itemID.'-'.$mat_ids[ucwords($mat['name'])]] = 
						array(
							'itemID' => $map->itemID,
							'matID' => $mat_ids[ucwords($mat['name'])],
							'abundance' => $mat['abundance'],
							'created_at' => $date,
							'updated_at' => $date
						);
					}
				}
			}

			var_dump($pos_insert);
			var_dump($mat_insert);

			// Bulk Inserts!
			if (!empty($pos_insert)){
				Pos::insert($pos_insert);
			} 
			
			Mat::insert($mat_insert);


			exit;

			return Redirect::route('import_tenerifis')
			               ->with('flash_msg', 'Import Successful');

		}
		else
		{
			return Redirect::route('import_tenerifis')
			               ->with('flash_error', 'Please supply copy+paste data')
			               ->withInput();
		}
	}

	public function importCatchView()
	{
		$this->layout = self::LAYOUT;
		$view = View::make(self::LAYOUT)
		            ->nest('navigation', 'navigation')
		            ->nest('footer', 'parts/footer')
		            ->nest('page_content', 'import/catch');

		return $view;
	}

	public function importCatchAction()
	{
		// DO REGISTRATION
		$import = Input::get('massImport');

		if($import !== false)
		{
			$final = array();
			$mat_ids = array_flip(Mat::$matTypes);

			$lines = explode("\n", $import);
			foreach($lines as $i => $line)
			{
				if(trim($line) == '')
				{
					continue;
				}

				$final[$i] = array(
					'name' => '',
					'mats' => array()
				);
				$sections = explode("\t", $line);
				foreach($sections as $x => $tab)
				{
					$mats = array();
					switch($x)
					{
						case 0;
							$final[$i]['name'] = trim($tab);
							break;

						case 1;
							$final[$i]['name'] .= ' '.self::intToRoman(intval(trim($tab)));
							break;

						case 2;
							$final[$i]['name'] .= ' - Moon '.trim($tab);
							break;

						case 3;
							$mats = array();
							$name = trim($tab);

							/// make sure its important
							if($name != '')
							{
								$mats['name'] = $name;
								$mats['abundance'] = 1;

								$final[$i]['mats'][] = $mats;
							}
							else if($name === "")
							{

								$mats['name'] = 'Barren';
								$mats['abundance'] = '1';
								$final[$i]['mats'][] = $mats;
							}
							break;

						case 4;
							$mats = array();
							$name = trim($tab);

							/// make sure its important
							if($name != '')
							{
								$mats['name'] = $name;
								$mats['abundance'] = 1;

								$final[$i]['mats'][] = $mats;
							}
							break;

						case 5;
							$mats = array();
							$name = trim($tab);

							/// make sure its important
							if($name != '')
							{
								$mats['name'] = $name;
								$mats['abundance'] = 1;

								$final[$i]['mats'][] = $mats;
							}
							break;

						case 6;
							$mats = array();
							$name = trim($tab);

							/// make sure its important
							if($name != '')
							{
								$mats['name'] = $name;
								$mats['abundance'] = 1;

								$final[$i]['mats'][] = $mats;
							}
							break;
					}
				}
			}

			$i = 0;
			foreach($final as $i => $moon)
			{
				$data = MapItem::where('itemName', '=', $moon['name'])->where('groupID', '=', Pos::$groupID)->get();
				if(isset($data[0]))
				{
					$map = $data[0];
					$pos = Pos::find($map->itemID);
					if(!empty($pos))
					{
						// Update Materials
						Mat::where('itemID', '=', $pos->itemID)->delete();
						foreach($moon['mats'] as $mat)
						{
							if(!isset($mat_ids[ucwords($mat['name'])]))
							{
								echo "Error Found on line ".($i+1)." of the spreadsheet: '".$mat['name']."' Is not a valid material name.";
								exit;
							}

							Mat::create(
								array(
									'itemID' => $pos->itemID,
									'matID' => $mat_ids[ucwords($mat['name'])],
									'abundance' => $mat['abundance']
								)
							);
						}

					}
					else
					{
						// insert
						Pos::create(
							array(
								'itemID' => $map->itemID,
								'type' => 0,
								'status' => 0,
								'password' => '',
								'owner' => '',
								'dscan' => '',
							)
						);

						foreach($moon['mats'] as $mat)
						{
							if(!isset($mat_ids[ucwords($mat['name'])]))
							{
								Echo "Error Found on line ".($i+1)." of the spreadsheet: '".$mat['name']."' Is not a valid material name.";
								exit;
							}

							Mat::create(
								array(
									'itemID' => $map->itemID,
									'matID' => $mat_ids[ucwords($mat['name'])],
									'abundance' => $mat['abundance']
								)
							);
						}
					}
				}
			}

			return Redirect::route('import_catch')
			               ->with('flash_msg', 'Import Successful');

		}
		else
		{
			return Redirect::route('import_catch')
			               ->with('flash_error', 'Please supply copy+paste data')
			               ->withInput();
		}
	}

	// --------------------------------------

	public function importReconView()
	{
		$this->layout = self::LAYOUT;
		$view = View::make(self::LAYOUT)
		            ->nest('navigation', 'navigation')
		            ->nest('footer', 'parts/footer')
		            ->nest('page_content', 'import/recon');

		return $view;
	}


	public function importReconAction()
	{
		// DO REGISTRATION
		$import = Input::get('massImport');

		if($import !== false)
		{
			$final = array();
			$mat_ids = array_flip(Mat::$matTypes);

			$lines = explode("\n", $import);
			foreach($lines as $i => $line)
			{
				if(trim($line) == '')
				{
					continue;
				}

				$final[$i] = array(
					'name' => '',
					'mats' => array()
				);
				$sections = str_getcsv($line, "\t");
				foreach($sections as $x => $tab)
				{
					$mats = array();
					switch($x)
					{
						case 0;
							// timestamp
							break;

						case 1;
							// const
							break;

						// Celestial, System Name
						case 2;
							$final[$i]['name'] = trim($tab);
							break;

						// Celestial, Planet Index
						case 3;
							$final[$i]['name'] .= ' '.self::intToRoman(intval(trim($tab)));
							break;

						// Celestial, Moon Index
						case 4;
							$final[$i]['name'] .= ' - Moon '.trim($tab);
							break;

						// Owner, Corp
						case 5;
							$final[$i]['owner'] = trim($tab);
							break;

						// Owner, Alliance
						case 6;
							$final[$i]['owner'] .= ' - '.trim($tab);
							break;

						// dscan
						case 7;
							$final[$i]['dscan'] = trim($tab);
							break;

						// notes line 1
						case 8;
							$final[$i]['notes'] = trim($tab);
							break;

						// notes line 2
						case 9;
							$final[$i]['notes'] .= "\n\n".trim($tab);
							break;

						// Scan Reporter, notes line 3
						case 10;
							$final[$i]['notes'] .= "\n\nReported By: ".trim($tab);
							break;

						// Materials
						case 11;
							$mats = array();
							if (trim($tab) !== '') {

								$data = preg_split( "/(,|\t)/", trim($tab) );
								foreach ($data as $mat_name)
								{
									if($data === "Empty" or $data === "Barren" or $data === "")
									{
										$mats['name'] = 'Barren';
										$mats['abundance'] = '1';
										$final[$i]['mats'][] = $mats;
									}
									else
									{
										$abundancy = substr($mat_name, -1);
										$name = substr($mat_name, 0, -2);

										// make sure its important
										if($name != '')
										{
											$mats['name'] = trim($name);
											$mats['abundance'] = $abundancy;

											$final[$i]['mats'][] = $mats;
										}
									}
								}
							}
							break;

						// Structure
						case 12;
							if($tab === "Empty")
							{
								$final[$i]['type'] = 0;
							}
							else
							{

								$towerTypes = array(
									'Empty' => '0',

									'Amarr Small' => 20060,
									'Caldari Small' => 20062,
									'Gallente Small' => 20064,
									'Minmatar Small' => 20066,

									'Amarr Medium' => 20059,
									'Caldari Medium' => 20061,
									'Gallente Medium' => 20063,
									'Minmatar Medium' => 20065,

									'Amarr Large' => 12235,
									'Caldari Large' => 16213,
									'Gallente Large' => 12236,
									'Minmatar Large' => 16214,

									'Faction - Blood Large' => 27530,
									'Faction - Blood Medium' => 27589,
									'Faction - Blood Small' => 27592,

									'Faction - Dark Blood Large' => 27532,
									'Faction - Dark Blood Medium' => 27591,
									'Faction - Dark Blood Small' => 27594,

									'Faction - Guristas Large' => 27533,
									'Faction - Guristas Medium' => 27595,
									'Faction - Guristas Small' => 27598,

									'Faction - Dread Guristas Large' => 27535,
									'Faction - Dread Guristas Medium' => 27597,
									'Faction - Dread Guristas Small' => 27600,

									'Faction - Serpentis Large' => 27536,
									'Faction - Serpentis Medium' => 27601,
									'Faction - Serpentis Small' => 27604,

									'Faction - Shadow Large' => 27538,
									'Faction - Shadow Medium' => 27603,
									'Faction - Shadow Small' => 27606,

									'Faction - Angel Large' => 27539,
									'Faction - Angel Medium' => 27607,
									'Faction - Angel Small' => 27610,

									'Faction - Domination Large' => 27540,
									'Faction - Domination Medium' => 27609,
									'Faction - Domination Small' => 27612,

									'Faction - Sansha Large' => 27780,
									'Faction - Sansha Medium' => 27782,
									'Faction - Sansha Small' => 27784,

									'Faction - True Sansha Large' => 27786,
									'Faction - True Sansha Medium' => 27788,
									'Faction - True Sansha Small' => 27790,
								);

								$final[$i]['type'] = $towerTypes[trim($tab)];
							}
							break;

						// Pos Status
						case 13;

							$tab = trim($tab);
							if ($tab === 'Yes') {
								$final[$i]['status'] = "1";
							}
							elseif ($tab === 'No, Anchored') {
								$final[$i]['status'] = "0";
							}
							elseif ($tab === 'No, Offline (Why are you filling this form out, go steal that shit.)') {
								$final[$i]['status'] = "0";
							}
							else {
								$final[$i]['status'] = "0";
							}

							break;
					}
				}
			}

			$i = 0;
			foreach($final as $i => $moon)
			{
				$data = MapItem::where('itemName', '=', $moon['name'])->where('groupID', '=', Pos::$groupID)->get();
				if(isset($data[0]))
				{
					$map = $data[0];
					$pos = Pos::find($map->itemID);

					// resolve owner to ID
					$owner = $moon['owner'];
					if ($owner !== '' and $owner !== ' - ') {
						$owner = explode(' - ', $owner)[0];
						$owner = str_replace(['[', ']'], '', $owner);
						$corp = Corporation::where('shortName', '=', $owner)->first();
						if (!empty($corp)) {
							$moon['owner'] = $corp->id;
						}
					}

					if(!empty($pos))
					{
						// Update POS Info
					    $pos->type   = (int) $moon['type'];
						$pos->status = (int) $moon['status'];
						$pos->owner = $moon['owner'];
						$pos->dscan = $moon['dscan'];
						$pos->notes = $moon['notes'];
						$pos->save();

						// Update Materials
						if (isset($moon['mats']) and !empty($moon['mats'])) {
							Mat::where('itemID', '=', $pos->itemID)->delete();
							foreach($moon['mats'] as $mat)
							{
								if(!isset($mat_ids[ucwords($mat['name'])]))
								{
									echo "Error Found on line ".($i+1)." of the spreadsheet: '".$mat['name']."' Is not a valid material name.";
									exit;
								}

								Mat::create(
									array(
										'itemID' => $pos->itemID,
										'matID' => $mat_ids[ucwords($mat['name'])],
										'abundance' => $mat['abundance']
									)
								);
							}
						}

					}
					else
					{
						// insert
						Pos::create(
							array(
								'itemID' => $map->itemID,
								'type' => (int) $moon['type'],
								'status' => (int) $moon['status'],
								'password' => '',
								'owner' => $moon['owner'],
								'dscan' => $moon['dscan'],
								'notes' => $moon['notes'],
							)
						);

						if (isset($moon['mats']) and !empty($moon['mats'])) {
							foreach($moon['mats'] as $mat)
							{
								if(!isset($mat_ids[ucwords($mat['name'])]))
								{
									Echo "Error Found on line ".($i+1)." of the spreadsheet: '".$mat['name']."' Is not a valid material name.";
									exit;
								}

								Mat::create(
									array(
										'itemID' => $map->itemID,
										'matID' => $mat_ids[ucwords($mat['name'])],
										'abundance' => $mat['abundance']
									)
								);
							}
						}
					}
				}
			}

			return Redirect::route('import_recon')
			               ->with('flash_msg', 'Import Successful!');

		}
		else
		{
			return Redirect::route('import_catch')
			               ->with('flash_error', 'Please supply copy+paste data')
			               ->withInput();
		}
	}

	// --------------------------------------

	public function importAnnaView()
	{
		$this->layout = self::LAYOUT;
		$view = View::make(self::LAYOUT)
		            ->nest('navigation', 'navigation')
		            ->nest('footer', 'parts/footer')
		            ->nest('page_content', 'import/anna');

		return $view;
	}

	public function importAnnaAction()
	{
		// DO REGISTRATION
		$import = Input::get('massImport');

		if($import !== false)
		{
			$final = array();
			$mat_ids = array_flip(Mat::$matTypes);

			$lines = explode("\n", $import);
			foreach($lines as $i => $line)
			{
				if(trim($line) == '')
				{
					continue;
				}

				$final[$i] = array(
					'name' => '',
					'mats' => array()
				);
				$sections = explode("\t", $line);
				foreach($sections as $x => $tab)
				{

					switch($x)
					{
						case 0;
							$final[$i]['name'] = trim($tab);
							break;

						case 1;
						case 2;
						case 3;
						case 4;
							$mats = array();
							$name = trim($tab);

							/// make sure its important
							if(strtoupper($name) != 'NONE' and strtoupper($name) != 'BARREN' and $name != "")
							{
								if(stristr($tab, "x2") or stristr($tab, "x3") or stristr($tab, "x4"))
								{
									$parts = explode(' x', $tab);

									// check
									if(count($parts) < 2)
									{
										echo "Error Found on line ".($i+1)." of the spreadsheet: abundancy modifier is not in proper format";
										exit;
									}

									$mats['name'] = trim($parts[0]);
									$mats['abundance'] = (int)$parts[1];
								}
								else
								{
									$mats['name'] = trim($tab);
									$mats['abundance'] = 1;
								}

								$final[$i]['mats'][] = $mats;
							}
							else if($name === "")
							{
								// skip
							}
							else
							{
								if($x == 1)
								{
									$mats['name'] = 'Barren';
									$mats['abundance'] = '1';
									$final[$i]['mats'][] = $mats;
								}
							}
							break;
					}
				}
			}

			$i = 0;
			foreach($final as $i => $moon)
			{
				$data = MapItem::where('itemName', '=', $moon['name'])->where('groupID', '=', Pos::$groupID)->get();
				if(isset($data[0]))
				{
					$map = $data[0];
					$pos = Pos::find($map->itemID);
					if(!empty($pos))
					{
						// Update Materials
						Mat::where('itemID', '=', $pos->itemID)->delete();
						foreach($moon['mats'] as $mat)
						{
							if(!isset($mat_ids[ucwords($mat['name'])]))
							{
								echo "Error Found on line ".($i+1)." of the spreadsheet: '".$mat['name']."' Is not a valid material name.";
								exit;
							}

							Mat::create(
							   array(
								   'itemID' => $pos->itemID,
								   'matID' => $mat_ids[ucwords($mat['name'])],
								   'abundance' => $mat['abundance']
							   )
							);
						}

					}
					else
					{
						// insert
						Pos::create(
						   array(
							   'itemID' => $map->itemID,
							   'type' => 0,
							   'status' => 0,
							   'password' => '',
							   'owner' => '',
							   'dscan' => '',
						   )
						);


						foreach($moon['mats'] as $mat)
						{
							if(!isset($mat_ids[ucwords($mat['name'])]))
							{
								Echo "Error Found on line ".($i+1)." of the spreadsheet: '".$mat['name']."' Is not a valid material name.";
								exit;
							}

							Mat::create(
							   array(
								   'itemID' => $map->itemID,
								   'matID' => $mat_ids[ucwords($mat['name'])],
								   'abundance' => $mat['abundance']
							   )
							);
						}
					}
				}
			}

			return Redirect::route('import_anna')
			               ->with('flash_msg', 'Import Successful');

		}
		else
		{
			return Redirect::route('import_anna')
			               ->with('flash_error', 'Please supply copy+paste data')
			               ->withInput();
		}
	}

	// --------------------------------------

	public function importFalconsView()
	{
		$this->layout = self::LAYOUT;
		$view = View::make(self::LAYOUT)
		            ->nest('navigation', 'navigation')
		            ->nest('footer', 'parts/footer')
		            ->nest('page_content', 'import/falcons');

		return $view;
	}

	public function importFalconsAction()
	{
		// DO REGISTRATION
		$import = Input::get('massImport');

		if($import !== false)
		{
			$final = array();
			$mat_ids = array_flip(Mat::$matTypes);

			$lines = explode("\n", $import);
			foreach($lines as $i => $line)
			{
				if(trim($line) == '')
				{
					continue;
				}

				$final[$i] = array(
					'name' => '',
					'type' => '',
					'status' => '',
					'password' => '',
					'owner' => '',
					'dscan' => '',
					'notes' => '',
					'mats' => array()
				);

				$sections = explode("\t", $line);
				foreach($sections as $x => $tab)
				{
					switch($x)
					{
						// System Name
						case 2:
							$final[$i]['system'] = trim($tab);
							break;

						// Planet Name
						case 3:
							$final[$i]['planet'] = trim($tab);
							break;

						// Moon Name
						case 4:
							$moon = trim($tab);

							// is roman numeral
							if(!is_numeric($moon))
							{
								$moon = self::unfuckRomanNumbersJesus($moon);
							}

							$final[$i]['moon'] = $moon;

							$final[$i]['name'] = $final[$i]['system'].' '.$final[$i]['planet'].' - Moon '.$final[$i]['moon'];

							break;

						// Tower Type
						case 5:
							$final[$i]['type'] = (trim(strtolower($tab)) == 'yes' ? '12236' : '0'); // omg falcons why didn't you record POS type omg
							break;

						// Materials on each moon
						case 6:
							$name = trim($tab);
							$mats = array();

							$mats_name = explode(',', $name);
							sort($mats_name, SORT_NATURAL | SORT_FLAG_CASE);
							foreach($mats_name as $mname)
							{
								/// make sure its important
								if(strtoupper($mname) != 'NONE' and strtoupper($mname) != 'BARREN' and $mname != "")
								{
									$mats['name'] = trim($mname);
									$mats['abundance'] = 1;

									if(!isset($mat_ids[ucwords($mats['name'])]))
									{
										echo "Error Found on line ".($i+1)." of the spreadsheet: '".$mats['name']."' Is not a valid material name.";
										exit;
									}
									$mats['id'] = $mat_ids[ucwords($mats['name'])];

									$final[$i]['mats'][] = $mats;
								}
							}
							break;

						// Pos Owner
						case 7:
							$final[$i]['owner'] = trim($tab);
							break;

						// Status of pos (Offline/Online)
						case 8:
							$final[$i]['status'] = (trim(strtolower($tab)) == 'yes' ? '1' : '0');
							break;

						// Dscan for pos
						case 9:
							$final[$i]['dscan'] = trim($tab);
							break;

						// Abundance data for first mat
						case 10:
							$name = trim($tab);

							if(intval($name) != 0 and isset($final[$i]['mats'][0]))
							{
								$final[$i]['mats'][0]['abundance'] = intval($name);
							}
							elseif(isset($final[$i]['mats'][0]))
							{
								$final[$i]['mats'][0]['abundance'] = 1;
							}
							break;

						// Abundance data for second mat
						case 11:
							$name = trim($tab);

							if(intval($name) != 0 and isset($final[$i]['mats'][1]))
							{
								$final[$i]['mats'][1]['abundance'] = intval($name);
							}
							elseif(isset($final[$i]['mats'][1]))
							{
								$final[$i]['mats'][0]['abundance'] = 1;
							}
							break;

						// Abundance data for third mat
						case 12:
							$name = trim($tab);

							if(intval($name) != 0 and isset($final[$i]['mats'][2]))
							{
								$final[$i]['mats'][2]['abundance'] = intval($name);
							}
							elseif(isset($final[$i]['mats'][2]))
							{
								$final[$i]['mats'][0]['abundance'] = 1;
							}
							break;

						// Abundance data for fourth mat
						case 13:
							$name = trim($tab);

							if(intval($name) != 0 and isset($final[$i]['mats'][3]))
							{
								$final[$i]['mats'][3]['abundance'] = intval($name);
							}
							elseif(isset($final[$i]['mats'][3]))
							{
								$final[$i]['mats'][0]['abundance'] = 1;
							}
							break;

						case 15:
							$final[$i]['notes'] = trim($tab);
							break;
					}
				}
			}

			$i = 0;
			foreach($final as $i => $moon)
			{
				echo $moon['name'].' -> ';
				$data = MapItem::where('itemName', '=', $moon['name'])->where('groupID', '=', Pos::$groupID)->get();
				if(isset($data[0]))
				{
					$map = $data[0];

					$pos = Pos::find($map->itemID);
					if(!empty($pos))
					{
						// Update Materials
						Mat::where('itemID', '=', $map->itemID)->delete();
						foreach($moon['mats'] as $mat)
						{
							Mat::create(
							   array(
								   'itemID' => $map->itemID,
								   'matID' => $mat['id'],
								   'abundance' => $mat['abundance']
							   )
							);
						}
					}
					else
					{
						// insert
						Pos::create(
							array(
								'itemID'   => $map->itemID,
								'type'     => $moon['type'],
								'status'   => $moon['status'],
								'password' => '',
								'owner'    => $moon['owner'],
								'dscan'    => $moon['dscan'],
								'notes'    => $moon['notes'],
							)
						);

						foreach($moon['mats'] as $mat)
						{
							Mat::create(
							   array(
								   'itemID' => $map->itemID,
								   'matID' => $mat['id'],
								   'abundance' => $mat['abundance']
							   )
							);
						}
					}
				}
			}

			return Redirect::route('import_falcons')
			               ->with('flash_msg', 'Import Successful!!!1!111one!11!1oneone');
		}
		else
		{
			return Redirect::route('import_falcons')
			               ->with('flash_error', 'Please supply copy+paste data')
			               ->withInput();
		}
	}

	// --------------------------------------

	private static function intToRoman($integer, $upcase = true)
	{
		$table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1);
		$return = '';
		while($integer > 0)
		{
			foreach($table as $rom=>$arb)
			{
				if($integer >= $arb)
				{
					$integer -= $arb;
					$return .= $rom;
					break;
				}
			}
		}

		return $return;
	}

	private static function unfuckRomanNumbersJesus($roman)
	{
		$romans = array(
			'M' => 1000,
			'CM' => 900,
			'D' => 500,
			'CD' => 400,
			'C' => 100,
			'XC' => 90,
			'L' => 50,
			'XL' => 40,
			'X' => 10,
			'IX' => 9,
			'V' => 5,
			'IV' => 4,
			'I' => 1,
		);

		$result = 0;

		foreach ($romans as $key => $value) {
			while (strpos($roman, $key) === 0) {
				$result += $value;
				$roman = substr($roman, strlen($key));
			}
		}
		return $result;
	}
}
<?php

class PocoController extends BaseController
{
	const LAYOUT = 'layouts.home';

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function insertView($id)
	{
		$celestial = MapItem::find($id);

		// moon does not exist
		if($celestial == false or $celestial->groupID != Poco::$groupID)
		{
			return Redirect::route('home')
		       ->with('flash_error', 'POCO does not exist');
		}

		$this->layout = self::LAYOUT;
		$view = View::make(self::LAYOUT)
	        ->nest('navigation', 'navigation')
	        ->nest('footer', 'parts/footer')
	        ->nest('page_content', 'celestial/poco/add',
	               array(
		               'celestial' => $celestial
	               )
		);
		return $view;
	}

	public function editView($id)
	{
		$poco = Poco::getByID($id);

		if($poco == false)
		{
			return Redirect::route('home')
		       ->with('flash_error', 'POCO does not exist');
		}

		$this->layout = self::LAYOUT;
		$view = View::make(self::LAYOUT)
	        ->nest('navigation', 'navigation')
	        ->nest('footer', 'parts/footer')
	        ->nest('page_content', 'celestial/poco/edit',
	               array(
		               'poco' => $poco
	               )
		);
		return $view;
	}

	public function insertAction($id)
	{
		$rules = array(
			'owner' => 'required',
			'tax' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails())
		{
			return Redirect::route('poco_add', array($id))
		       ->with('flash_error', 'Please fill in all POCO Information')
		       ->withInput();
		}
		else
		{
			// DO REGISTRATION
			$map = MapItem::find(Input::get('itemID'));
			if($map !== false)
			{
				$tax = (double)Input::get('tax');
				if($tax < 0)
				{
					$tax = 0;
				}

				if($tax > 100)
				{
					$tax = 100;
				}

				$owner = Input::get('owner');
				if (Input::get('corporationID') != '') {
					$owner = Input::get('corporationID');
				}

				Poco::create(
					array(
						'itemID' => $map->itemID,
						'owner' => $owner,
						'tax' => $tax,
					)
				);

				return Redirect::route('system', array($map->solarSystemID))
			       ->with('flash_msg', 'New POCO "'.$map->itemName.'" Successfully Added')
			       ->withInput();
			}
			else
			{
				return Redirect::route('home')
			       ->with('flash_error', 'This moon already exists in the Database')
			       ->withInput();
			}
		}
	}

	public function editAction($id)
	{
		$rules = array(
			'owner' => 'required',
			'tax' => 'required',
		);

		$poco = Poco::getByID($id);

		if($poco == false)
		{
			return Redirect::route('home')
		       ->with('flash_error', 'POCO does not exist')
		       ->withInput();
		}

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::route('poco_edit', array($id))
		       ->with('flash_error', 'Please fill in all POCO Information')
		       ->withInput();
		}
		else
		{
			// DO REGISTRATION
			$map = MapItem::find(Input::get('itemID'));
			if($map !== false)
			{

				$tax = (double)Input::get('tax');
				if($tax < 0)
				{
					$tax = 0;
				}

				if($tax > 100)
				{
					$tax = 100;
				}

				$owner = Input::get('owner');
				if (Input::get('corporationID') != '') {
					$owner = Input::get('corporationID');
				}

				$poco->tax = $tax;
				$poco->owner = $owner;

				// save everything
				$poco->save();

				// GTFO
				return Redirect::route('system', array($poco->solarSystemID))
			       ->with('flash_msg', 'POCO "'.$poco->itemName.'" Changes Saved')
			       ->withInput();
			}
			else
			{
				return Redirect::route('home')
			       ->with('flash_error', 'This moon already exists in the Database');
			}
		}
	}

	public function deleteAction($id)
	{
		$poco = Poco::getByID($id);

		if($poco == false)
		{
			return Redirect::route('home')
		       ->with('flash_error', 'POCO does not exist')
		       ->withInput();
		}

		// delete that shit
		Poco::find($id)->delete();

		return Redirect::route('system', array($poco->solarSystemID))
	       ->with('flash_msg', 'POCO "'.$poco->itemName.'" Was Deleted')
	       ->withInput();
	}

}
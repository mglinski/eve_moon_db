<?php

class PosController extends BaseController
{
	const LAYOUT = 'layouts.home';

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function insertView($id)
	{
		$celestial = MapItem::find($id);

		// moon does not exist
		if($celestial == false or $celestial->groupID != Pos::$groupID)
		{
			return Redirect::route('home')
		       ->with('flash_error', 'POS does not exist');
		}

		$this->layout = self::LAYOUT;
		$view = View::make(self::LAYOUT)
	        ->nest('navigation', 'navigation')
	        ->nest('footer', 'parts/footer')
	        ->nest('page_content', 'celestial/pos/add',
	               [
		               'celestial' => $celestial
	               ]
		);
		return $view;
	}

	public function editView($id)
	{
		$pos = Pos::getByID($id);

		if($pos == false)
		{
			return Redirect::route('home')
		       ->with('flash_error', 'POS does not exist');
		}

		$this->layout = self::LAYOUT;
		$view = View::make(self::LAYOUT)
	        ->nest('navigation', 'navigation')
	        ->nest('footer', 'parts/footer')
	        ->nest('page_content', 'celestial/pos/edit',
				[
				   'pos' => $pos
			]
		);
		return $view;
	}

	public function insertAction($id)
	{
		$rules = [
			'type' => 'required',
			//'status' => 'required',
			//'owner' => 'required',
			//'mat' => 'required',
			//'function' => 'required',
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails())
		{
			return Redirect::route('pos_add', [$id])
		       ->with('flash_error', 'Please fill in all POS Information')
		       ->withInput();
		}
		else
		{
			// DO REGISTRATION
			$map = MapItem::find(Input::get('itemID'));
			if($map !== false)
			{

				$owner = Input::get('owner');
				if (Input::get('corporationID') != '') {
					$owner = Input::get('corporationID');
				}

				Pos::create(
					[
						'itemID' => $map->itemID,
						'type' => (int)Input::get('type'),
						'status' => (int)Input::get('status'),
						'password' => Input::get('password'),
						'owner' => $owner,
						'dscan' => Input::get('dscan'),
						'notes' => Input::get('notes'),
					]
				);

				// add mats to DB
				$mats = Input::get('mat');
				$abundance = Input::get('mat_abundance');
				if($mats != false)
				{
					foreach($mats as $i => $matID)
					{
						Mat::create(
							[
								'itemID' => $map->itemID,
								'matID' => (int)$matID,
								'abundance' => (int)$abundance[$i]
							]
						);
					}
				}

				// add functions to DB
				$functions = Input::get('function');
				if($functions != false)
				{
					foreach($functions as $functionID)
					{
						Functions::create(
							[
								'itemID' => $map->itemID,
								'functionID' => (int)$functionID
							]
						);
					}
				}

				return Redirect::route('system', [$map->solarSystemID])
					->with('flash_msg', 'New POS "'.$map->itemName.'" Successfully Added')
					->withInput();
			}
			else
			{
				return Redirect::route('home')
					->with('flash_error', 'This moon already exists in the Database')
					->withInput();
			}
		}
	}

	public function editAction($id)
	{
		$rules = [
			'type' => 'required',
			//'status' => 'required',
			//'owner' => 'required',
			//'mat' => 'required',
			//'function' => 'required',
		];

		$pos = Pos::getByID($id);

		if($pos == false)
		{
			return Redirect::route('home')
		       ->with('flash_error', 'POS does not exist')
		       ->withInput();
		}

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::route('pos_edit', [$id])
		       ->with('flash_error', 'Please fill in all POS Information')
		       ->withInput();
		}
		else
		{
			// DO REGISTRATION
			$map = MapItem::find(Input::get('itemID'));
			if($map !== false)
			{
				$owner = Input::get('owner');
				if (Input::get('corporationID') != '') {
					$owner = Input::get('corporationID');
				}

				$pos->type   = (int) Input::get('type');
				$pos->status = (int)Input::get('status');
				$pos->password = Input::get('password');
				$pos->owner = $owner;
				$pos->dscan = Input::get('dscan');
				$pos->notes = Input::get('notes');

				// -------------------------------

				// Replace POS Mats
				Mat::where('itemID', '=', $pos->itemID)->delete();

				// add mats to DB
				$mats = Input::get('mat');
				$abundance = Input::get('mat_abundance');
				if($mats != false)
				{
					foreach($mats as $i => $matID)
					{
						$mat = Mat::create(
							[
								'itemID' => $map->itemID,
								'matID' => (int)$matID,
								'abundance' => (int)$abundance[$i]
							]
						);
					}
				}

				// -------------------------------

				// Replace POS Roles
				Functions::where('itemID', '=', $pos->itemID)->delete();

				// add functions to DB
				$functions = Input::get('function');
				if($functions != false)
				{
					foreach($functions as $functionID)
					{
						Functions::create(
							[
								'itemID' => $map->itemID,
								'functionID' => (int)$functionID
							]
						);
					}
				}

				// -------------------------------

				// save everything
				$pos->save();

				// GTFO
				return Redirect::route('system', [$pos->solarSystemID])
				       ->with('flash_msg', 'POS "'.$pos->itemName.'" Changes Saved')
				       ->withInput();
			}
			else
			{
				return Redirect::route('home')
				       ->with('flash_error', 'This moon already exists in the Database');
			}
		}
	}

	public function deleteAction($id)
	{
		$pos = Pos::getByID($id);

		if($pos == false)
		{
			return Redirect::route('home')
		       ->with('flash_error', 'POS does not exist')
		       ->withInput();
		}

		// delete that shit
		Mat::where('itemID', '=', $pos->itemID)->delete();
		Functions::where('itemID', '=', $pos->itemID)->delete();
		Pos::find($id)->delete();

		return Redirect::route('system', [$pos->solarSystemID])
	       ->with('flash_msg', 'POS "'.$pos->itemName.'" Was Deleted')
	       ->withInput();
	}
}
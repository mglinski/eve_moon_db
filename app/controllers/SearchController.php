<?php

class SearchController extends BaseController
{
	const LAYOUT = 'layouts.home';

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function universeSearchMethod()
	{
		$regionID = Input::get('region', false);
		$materialID = Input::get('material', false);

		if($regionID != false and $materialID != false)
		{
			$regions = RegionItem::where('regionID', '<', '11000001')->whereNotIn('regionID', RegionItem::$joveRegions)->orderBy('regionName', 'asc')->get();
			$celestials = MapItem::join('mglinski_moons.pos_mats', 'pos_mats.itemID', '=', 'mapDenormalize.itemID')
				->where('pos_mats.matID',           '=', $materialID)
				->where('mapDenormalize.regionID',  '=', $regionID)
				->where('mapDenormalize.groupID',   '=', 8)
				->where('mapDenormalize.security',  '<', 1.0)
				->orderBy('mapDenormalize.itemID',       'asc')->get();

			$materials = Mat::$matTypes;

			$this->layout = self::LAYOUT;
			$view = View::make(self::LAYOUT)
				->nest('navigation', 'navigation')
				->nest('footer', 'parts/footer')
				->nest('page_content', 'celestial/search/result',
					array(
						'celestials' => $celestials
					)
				);
			return $view;
		}
		else
		{

			$regions = RegionItem::where('regionID', '<', '11000001')->whereNotIn('regionID', RegionItem::$joveRegions)->orderBy('regionName', 'asc')->get();
			$materials = Mat::$matTypes;

			$this->layout = self::LAYOUT;
			$view = View::make(self::LAYOUT)
				->nest('navigation', 'navigation')
				->nest('footer', 'parts/footer')
				->nest('page_content', 'celestial/search/search',
					array(
						'regions' => $regions,
						'materials' => $materials,
					)
				);
			return $view;
		}
	}

	// ---------------------------------------

	public function searchCelestialAction()
	{
		$line = new stdClass();
		$line->locations = array();
		$text = Input::get('q');

		if(!$text)
		{
			App::abort(404);
			exit;
		}

		// did we get passed an ID?
		if(ctype_digit($text))
		{
			$result = MapItem::where('itemID', '=', $text)->whereIn('groupID', array('7', '8'))->where('security', '<', '0.8')->take(1)->get();
			if($result != false)
			{
				$result = $result[0];
				$line->locations = array(
					'text' => $result->itemName,
					'id' => $result->itemID
				);
				goto endGetLocation;
				// OH GOD AAAAAAAAAAHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
				// MY EYES MY EYES ARE BURNING WITH ACID IT HURTS OHHHHH GODDDD IT HUURRRTTTTTSSSSSSSss..
			}
		}
		$search = MapItem::where('itemName', 'like', $text.'%')->whereIn('groupID', array('7', '8'))->where('security', '<', '0.8')->take(40)->get();
		foreach($search as $row)
		{
			$line->locations[] = array(
				'text' => $row->itemName,
				'id' => $row->itemID
			);
		}
		// GOTO END
		endGetLocation:
		if(!Input::has('callback'))
		{
			return Response::json($line);
		}
		else
		{
			return Response::json($line)->setCallback(Input::get('callback'));
		}
	}


	public function searchCorpsAction()
	{
		$line = new stdClass();
		$line->locations = array();
		$text = Input::get('q');

		if(!$text)
		{
			App::abort(404);
			exit;
		}

		// did we get passed an ID?
		if(ctype_digit($text))
		{
			$result = Corporation::select(
					'corporation.id as corporationID', 'corporation.name as corporationName',
					'corporation.shortName as corporationShortName', 'alliance.id as allianceID',
					'alliance.name as allianceName', 'alliance.shortName as allianceShortName'
				)
				->where('corporation.id', '=', $text)
				->leftJoin('alliance', 'alliance.id', '=', 'corporation.allianceID')
				->take(1)->get();
			if($result != false)
			{
				$row = $result[0];

				$name = $row->corporationName.' ['.$row->corporationShortName.']';
				if ($row->allianceID != 0)
					$name .= ' ('.$row->allianceName.' ['.$row->allianceShortName.'])';

				$line->locations = array(
					'text' => $name,
					'id' => $row->corporationID
				);
				goto endGetLocation;
				// OH GOD AAAAAAAAAAHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
				// MY EYES MY EYES ARE BURNING WITH ACID IT HURTS OHHHHH GODDDD IT HUURRRTTTTTSSSSSSSss..
			}
		}
		$search = Corporation::select(
				'corporation.id as corporationID', 'corporation.name as corporationName',
				'corporation.shortName as corporationShortName', 'alliance.id as allianceID',
				'alliance.name as allianceName', 'alliance.shortName as allianceShortName'
			)
			->where('corporation.name', 'like', '%'.$text.'%')->orWhere('corporation.shortName', 'like', '%'.$text.'%')
			->orWhere('alliance.name', 'like', '%'.$text.'%')->orWhere('alliance.shortName', 'like', '%'.$text.'%')
			->leftJoin('alliance', 'alliance.id', '=', 'corporation.allianceID')
			->take(20)->get();
		foreach($search as $row)
		{
			// build corp name
			$name = $row->corporationName.' ['.$row->corporationShortName.']';
			if ($row->allianceID != 0)
				$name .= ' ('.$row->allianceName.' ['.$row->allianceShortName.'])';

			$line->locations[] = array(
				'text' => $name,
				'id' => $row->corporationID
			);
		}
		// GOTO END
		endGetLocation:
		if(!Input::has('callback'))
		{
			return Response::json($line);
		}
		else
		{
			return Response::json($line)->setCallback(Input::get('callback'));
		}
	}

}
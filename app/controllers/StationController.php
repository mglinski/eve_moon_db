<?php

class StationController extends BaseController
{
	const LAYOUT = 'layouts.home';

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function editView($id)
	{
		$station = Station::find($id);

		if($station == false)
		{
			return Redirect::route('home')
		       ->with('flash_error', 'Station does not exist');
		}

		$solarSystem = MapItem::find($station->solarSystemID);

		$this->layout = self::LAYOUT;
		$view = View::make(self::LAYOUT)
	        ->nest('navigation', 'navigation')
	        ->nest('footer', 'parts/footer')
	        ->nest('page_content', 'celestial/station/edit',
	               array(
		               'solarSystem' => $solarSystem,
		               'station' => $station
	               )
		);
		return $view;
	}



	public function editAction($id)
	{
		$rules = array(
			'timer' => 'required',
		);

		$station = Station::find($id);

		if($station == false)
		{
			return Redirect::route('home')
		       ->with('flash_error', 'Station does not exist')
		       ->withInput();
		}

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::route('station_edit', array($id))
		       ->with('flash_error', 'Please fill in all Station Information')
		       ->withInput();
		}
		else
		{
			$station->timer = Input::get('timer');
			$station->save();

			return Redirect::route('system', array($station->solarSystemID))
				->with('flash_msg', 'Station "'.$station->name.'" Successfully Edited')
				->withInput();
		}
	}

}
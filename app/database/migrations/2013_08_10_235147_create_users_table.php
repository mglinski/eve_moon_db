<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('users', function($t)
		{
			$t->increments('id')->unsigned();
			$t->string('username', 64);
			$t->string('password', 64);
			$t->string('email', 255);
			$t->integer('status');
			$t->integer('permission');
			$t->string('remember_token', 100);
			$t->timestamps();
		});

		Schema::create('pos', function($t)
		{
			$t->bigInteger('itemID')->unsigned();
			$t->integer('type');
			$t->integer('status');
			$t->text('password');
			$t->text('owner');
			$t->text('dscan');
			$t->text('notes');
			$t->timestamps();
		});

		Schema::create('poco', function($t)
		{
			$t->bigInteger('itemID')->unsigned();
			$t->text('owner');
			$t->decimal('tax', 5, 2);
			$t->timestamps();
		});

		Schema::create('pos_mats', function($t)
		{
			$t->increments('id')->unsigned();
			$t->bigInteger('itemID')->unsigned();
			$t->integer('matID')->unsigned();
			$t->integer('abundance')->unsigned();
			$t->timestamps();
		});

		Schema::create('pos_functions', function($t)
		{
			$t->increments('id')->unsigned();
			$t->bigInteger('itemID')->unsigned();
			$t->integer('functionID')->unsigned();
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('users');
		Schema::drop('pos');
		Schema::drop('poco');
		Schema::drop('pos_mats');
		Schema::drop('pos_functions');
	}

}
<?php

use Illuminate\Database\Migrations\Migration;

class CreateIhubTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ihub', function($t)
		{
			$t->bigInteger('itemID')->unsigned();
			$t->bigInteger('solarSystemID')->unsigned();
			$t->text('owner');
			$t->integer('status');
			$t->text('timer');
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('ihub');
	}

}
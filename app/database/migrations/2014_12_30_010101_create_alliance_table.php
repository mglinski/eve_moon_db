<?php

use Illuminate\Database\Migrations\Migration;

class CreateAllianceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alliance', function($t)
		{
			$t->bigInteger('id')->unsigned()->primary();
			$t->string('name', 120)->index();
			$t->string('shortName', 5)->index();
			$t->bigInteger('executorCorpID')->unsigned();
			$t->integer('memberCount')->unsigned();
			$t->dateTime('startDate')->index();
			$t->dateTime('endDate')->index();

			$t->timestamps();
		});

		Schema::create('corporation', function($t)
		{
			$t->bigInteger('id')->unsigned()->primary();
			$t->string('name', 120)->index();
			$t->string('shortName', 5)->index();
			$t->bigInteger('ceoID')->unsigned();
			$t->string('ceoName', 120);
			$t->bigInteger('stationID')->unsigned();
			$t->string('stationName', 120);
			$t->text('description');
			$t->text('url');
			$t->integer('allianceID')->unsigned()->index();
			$t->integer('factionID')->unsigned()->index();
			$t->decimal('taxRate', 5, 2);
			$t->integer('shares')->unsigned();

			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('alliance');
		Schema::drop('corporation');
	}

}
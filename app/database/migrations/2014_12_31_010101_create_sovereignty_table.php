<?php

use Illuminate\Database\Migrations\Migration;

class CreateSovereigntyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sovereignty', function($t)
		{
			$t->bigInteger('id')->unsigned()->primary();
			$t->string('solarSystemName', 120)->index();
			$t->integer('allianceID')->unsigned()->index();
			$t->integer('factionID')->unsigned()->index();
			$t->integer('corporationID')->unsigned()->index();

			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('sovereignty');
	}

}
<?php

use Illuminate\Database\Migrations\Migration;

class CreateOutpostTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// kill current table for something better
		Schema::create('station', function($t)
		{
			$t->integer('id')->unsigned()->primary();
			$t->integer('itemID')->unsigned();

			$t->string('name', 255);
			$t->integer('type')->unsigned();
			$t->integer('solarSystemID')->unsigned();
			$t->integer('corporationID')->unsigned();
			$t->string('corporationName', 120);
			$t->text('timer');

			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// recreate old table
		Schema::drop('station');
	}

}
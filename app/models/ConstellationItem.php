<?php

class ConstellationItem extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'mapConstellations';

	/**
	 * The database connection used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'eve_data';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array();

}
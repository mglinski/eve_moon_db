<?php

class Corporation extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'corporation';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array(
		'id' ,
		'name',
		'shortName',
		'ceoID',
		'ceoName',
		'stationID',
		'stationName',
		'description',
		'url',
		'allianceID',
		'factionID',
		'taxRate',
		'shares',
	);

	public static function getCorp($id) {
		$result = Corporation::select(
			'corporation.id as corporationID', 'corporation.name as corporationName',
			'corporation.shortName as corporationShortName', 'alliance.id as allianceID',
			'alliance.name as allianceName', 'alliance.shortName as allianceShortName'
		)
			->where('corporation.id', '=', $id)
			->leftJoin('alliance', 'alliance.id', '=', 'corporation.allianceID')
			->take(1)->get();

		if (count($result) === 1)
			return $result[0];
		else
			return false;
	}

}
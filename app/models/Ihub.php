<?php

class Ihub extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ihub';

	/**
	 * The database column primary key.
	 *
	 * @var string
	 */
	protected $primaryKey = 'itemID';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	public static $groupID = 7;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array(
		'itemID',
		'solarSystemID',
		'owner',
		'timer',
		'status'
	);

	public static $statusTypes = array(
		'0' => 'Unanchored',
		'1' => 'Anchored',
		'2' => 'Online',
	);

	public static function getBySystemID($id)
	{
		$return = self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'ihub.itemID')
			->join('mglinski_eve_sde.mapRegions', 'mapRegions.regionID', '=', 'mapDenormalize.regionID')
			->join('mglinski_eve_sde.mapSolarSystems', 'mapSolarSystems.solarSystemID', '=', 'mapDenormalize.solarSystemID')
			->where('ihub.solarSystemID', '=', $id)
			->get();

		return $return;
	}

	public static function getByID($id)
	{
		$return = self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'ihub.itemID')
			->join('mglinski_eve_sde.mapRegions', 'mapRegions.regionID', '=', 'mapDenormalize.regionID')
			->join('mglinski_eve_sde.mapSolarSystems', 'mapSolarSystems.solarSystemID', '=', 'mapDenormalize.solarSystemID')
			->where('ihub.itemID', '=', $id)
			->get();

		if(isset($return[0]))
		{
			return $return[0];
		}
		return false;
	}

	public static function getCountOfMappedPlanetsInRegionByID($id)
	{
		return  self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'ihub.itemID')
		        ->where('mapDenormalize.regionID', '=', $id)
		        ->count();
	}

	public static function getCountOfMappedPlanetsInSystemByID($id)
	{
		return  self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'ihub.itemID')
		        ->where('mapDenormalize.solarSystemID', '=', $id)
		        ->count();
	}

	public static function getRecentUpdates($paginate = 5)
	{
		return self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'ihub.itemID')
			->join('mglinski_eve_sde.mapRegions', 'mapRegions.regionID', '=', 'mapDenormalize.regionID')
			->join('mglinski_eve_sde.mapSolarSystems', 'mapSolarSystems.solarSystemID', '=', 'mapDenormalize.solarSystemID')
			->orderBy('ihub.updated_at', 'desc')
			->paginate($paginate);
	}

	public static function getRecentUpdatesBySolarSystemID($systemID, $paginate = 5)
	{
		return self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'ihub.itemID')
			->join('mglinski_eve_sde.mapRegions', 'mapRegions.regionID', '=', 'mapDenormalize.regionID')
			->join('mglinski_eve_sde.mapSolarSystems', 'mapSolarSystems.solarSystemID', '=', 'mapDenormalize.solarSystemID')
			->orderBy('ihub.updated_at', 'desc')
			->where('mapDenormalize.solarSystemID', '=', $systemID)
			->paginate($paginate);
	}

	public static function getRecentUpdatesByRegionID($regionID, $paginate = 5)
	{
		return self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'ihub.itemID')
			->join('mglinski_eve_sde.mapRegions', 'mapRegions.regionID', '=', 'mapDenormalize.regionID')
			->join('mglinski_eve_sde.mapSolarSystems', 'mapSolarSystems.solarSystemID', '=', 'mapDenormalize.solarSystemID')
			->orderBy('ihub.updated_at', 'desc')
			->where('mapDenormalize.regionID', '=', $regionID)
			->paginate($paginate);
	}
}
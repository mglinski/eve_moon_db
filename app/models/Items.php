<?php

class Items extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'invitems';

	/**
	 * The database connection used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'eve_data';

	/**
	 * The database column primary key.
	 *
	 * @var string
	 */
	protected $primaryKey = 'typeID';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array();

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	public static $starbaseStructureGroupIDs = array(363, 311, 397, 404, 413, 416, 426, 449, 430, 417, 438, 439, 441, 440, 443, 444, 471, 473, 837, 838, 839, 709, 707, 1212);

	// get all starbase structures
	public static function getStarbaseStructures()
	{
		return self::whereIn('pos.itemID', self::$starbaseStructureGroupIDs)->get();
	}
}
<?php

class Mat extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'pos_mats';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array(
		'itemID',
		'matID',
		'abundance'
	);

	public static $matTypes = array(
		0 => 'Barren',
		16633 => 'Hydrocarbons',
		16634 => 'Atmospheric Gases',
		16635 => 'Evaporite Deposits',
		16636 => 'Silicates',
		16637 => 'Tungsten',
		16638 => 'Titanium',
		16639 => 'Scandium',
		16640 => 'Cobalt',
		16641 => 'Chromium',
		16642 => 'Vanadium',
		16643 => 'Cadmium',
		16644 => 'Platinum',
		16646 => 'Mercury',
		16647 => 'Caesium',
		16648 => 'Hafnium',
		16649 => 'Technetium',
		16650 => 'Dysprosium',
		16651 => 'Neodymium',
		16652 => 'Promethium',
		16653 => 'Thulium',
	);

	public static $matRarity = array(
		0 => '1',
		16633 => '1',
		16634 => '1',
		16635 => '1',
		16636 => '1',
		16637 => '2',
		16638 => '2',
		16639 => '2',
		16640 => '2',
		16641 => '3',
		16642 => '3',
		16643 => '3',
		16644 => '3',
		16646 => '4',
		16647 => '4',
		16648 => '4',
		16649 => '4',
		16650 => '5',
		16651 => '5',
		16652 => '5',
		16653 => '5',
	);

	public static $matRarityColor = array(
		'1' => '#999999',
		'2' => '#B0DB25',
		'3' => '#ffcc00',
		'4' => '#ff8800',
		'5' => '#ff2200',
	);

	public function pos()
	{
		return $this->belongsTo('Pos', 'itemID');
	}

	public static function getMaterialCountByRegion($regionID, $materials)
	{
		return MapItem::join('mglinski_moons.pos_mats', 'pos_mats.itemID', '=', 'mapDenormalize.itemID')
			->whereIn('pos_mats.matID',         $materials)
			->where('mapDenormalize.regionID',  '=', $regionID)
			->where('mapDenormalize.groupID',   '=', 8)
			->where('mapDenormalize.security',  '<', 1.0)
			->orderBy('mapDenormalize.itemID',  'asc')->count();
	}

	public static function getHTMLFromList($mats)
	{
		ob_start();

		if(count($mats) > 0)
		{
			foreach($mats as $mat)
			{
				/*
				if($x % 2 == 0 and $x != 0)
				{
					echo "<br />";
				}
				*/
				?><label style="background-color: <?=self::$matRarityColor[self::$matRarity[$mat->matID]]?>" class="label label-default <?php if($mat->matID <= 16636){ ?>hidden moonCrap<?php } ?>"><?=self::$matTypes[$mat->matID]?></label> <?php
			}
		}
		else
		{
			?>
			<label style="background-color: #111" class="label label-default">No Data</label>
			<?php
		}

		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
}
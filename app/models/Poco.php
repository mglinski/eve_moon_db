<?php

class Poco extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'poco';

	/**
	 * The database column primary key.
	 *
	 * @var string
	 */
	protected $primaryKey = 'itemID';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	public static $groupID = 7;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array(
		'itemID',
		'owner',
		'tax',
		'status'
	);

	public static $planetTypes = array(
		'11' => 'Temperate',
		'12' => 'Ice',
		'13' => 'Gas',
		'2014' => 'Oceanic',
		'2015' => 'Lava',
		'2016' => 'Barren',
		'2017' => 'Storm',
		'2063' => 'Plasma',
		'30889' => 'Shattered',
	);

	public static function getByID($id)
	{
		$return = self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'poco.itemID')
			->join('mglinski_eve_sde.mapRegions', 'mapRegions.regionID', '=', 'mapDenormalize.regionID')
			->join('mglinski_eve_sde.mapSolarSystems', 'mapSolarSystems.solarSystemID', '=', 'mapDenormalize.solarSystemID')
			->where('poco.itemID', '=', $id)
			->get();

		if(isset($return[0]))
		{
			return $return[0];
		}
		return false;
	}

	public static function getCountOfMappedPlanetsInRegionByID($id)
	{
		return  self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'poco.itemID')
		        ->where('mapDenormalize.regionID', '=', $id)
		        ->count();
	}

	public static function getCountOfMappedPlanetsInSystemByID($id)
	{
		return  self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'poco.itemID')
		        ->where('mapDenormalize.solarSystemID', '=', $id)
		        ->count();
	}

	public static function getRecentUpdates($paginate = 5)
	{
		return self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'poco.itemID')
			->join('mglinski_eve_sde.mapRegions', 'mapRegions.regionID', '=', 'mapDenormalize.regionID')
			->join('mglinski_eve_sde.mapSolarSystems', 'mapSolarSystems.solarSystemID', '=', 'mapDenormalize.solarSystemID')
			->orderBy('poco.updated_at', 'desc')
			->paginate($paginate);
	}

	public static function getRecentUpdatesBySolarSystemID($systemID, $paginate = 5)
	{
		return self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'poco.itemID')
			->join('mglinski_eve_sde.mapRegions', 'mapRegions.regionID', '=', 'mapDenormalize.regionID')
			->join('mglinski_eve_sde.mapSolarSystems', 'mapSolarSystems.solarSystemID', '=', 'mapDenormalize.solarSystemID')
			->orderBy('poco.updated_at', 'desc')
			->where('mapDenormalize.solarSystemID', '=', $systemID)
			->paginate($paginate);
	}

	public static function getRecentUpdatesByRegionID($regionID, $paginate = 5)
	{
		return self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'poco.itemID')
			->join('mglinski_eve_sde.mapRegions', 'mapRegions.regionID', '=', 'mapDenormalize.regionID')
			->join('mglinski_eve_sde.mapSolarSystems', 'mapSolarSystems.solarSystemID', '=', 'mapDenormalize.solarSystemID')
			->orderBy('poco.updated_at', 'desc')
			->where('mapDenormalize.regionID', '=', $regionID)
			->paginate($paginate);
	}
}
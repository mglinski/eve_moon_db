<?php

class Pos extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'pos';

	/**
	 * The database column primary key.
	 *
	 * @var string
	 */
	protected $primaryKey = 'itemID';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array(
		'itemID',
		'type',
		'status',
		'password',
		'owner',
		'dscan',
	);

	public static $towerStatusTypes = array(
		'0' => 'Offline',
		'1' => 'Online',
	);

	public static $groupID = 8;

	public static $towerTypes = array(
		'0' => 'Empty',

		'12235' => 'Amarr Control Tower',
		'20059' => 'Amarr Control Tower Medium',
		'20060' => 'Amarr Control Tower Small',

		'16213' => 'Caldari Control Tower',
		'20061' => 'Caldari Control Tower Medium',
		'20062' => 'Caldari Control Tower Small',

		'12236' => 'Gallente Control Tower',
		'20063' => 'Gallente Control Tower Medium',
		'20064' => 'Gallente Control Tower Small',

		'16214' => 'Minmatar Control Tower',
		'20065' => 'Minmatar Control Tower Medium',
		'20066' => 'Minmatar Control Tower Small',

		'27530' => 'Blood Control Tower',
		'27589' => 'Blood Control Tower Medium',
		'27592' => 'Blood Control Tower Small',

		'27532' => 'Dark Blood Control Tower',
		'27591' => 'Dark Blood Control Tower Medium',
		'27594' => 'Dark Blood Control Tower Small',

		'27533' => 'Guristas Control Tower',
		'27595' => 'Guristas Control Tower Medium',
		'27598' => 'Guristas Control Tower Small',

		'27535' => 'Dread Guristas Control Tower',
		'27597' => 'Dread Guristas Control Tower Medium',
		'27600' => 'Dread Guristas Control Tower Small',

		'27536' => 'Serpentis Control Tower',
		'27601' => 'Serpentis Control Tower Medium',
		'27604' => 'Serpentis Control Tower Small',

		'27538' => 'Shadow Control Tower',
		'27603' => 'Shadow Control Tower Medium',
		'27606' => 'Shadow Control Tower Small',

		'27539' => 'Angel Control Tower',
		'27607' => 'Angel Control Tower Medium',
		'27610' => 'Angel Control Tower Small',

		'27540' => 'Domination Control Tower',
		'27609' => 'Domination Control Tower Medium',
		'27612' => 'Domination Control Tower Small',

		'27780' => 'Sansha Control Tower',
		'27782' => 'Sansha Control Tower Medium',
		'27784' => 'Sansha Control Tower Small',

		'27786' => 'True Sansha Control Tower',
		'27788' => 'True Sansha Control Tower Medium',
		'27790' => 'True Sansha Control Tower Small',
	);

	public function mats()
	{
		return $this->hasMany('Mat', 'itemID');
	}

	public function functions()
	{
		return $this->hasMany('Functions', 'itemID');
	}

	public static function getByID($id)
	{
		$return = self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'pos.itemID')
	          ->join('mglinski_eve_sde.mapRegions', 'mapRegions.regionID', '=', 'mapDenormalize.regionID')
			  ->join('mglinski_eve_sde.mapSolarSystems', 'mapSolarSystems.solarSystemID', '=', 'mapDenormalize.solarSystemID')
	          ->with('mats', 'functions')
	          ->where('pos.itemID', '=', $id)
	          ->get();

		if(isset($return[0]))
		{
			return $return[0];
		}

		return false;
	}

	public static function getCountOfMappedMoonsInSystemByID($id)
	{
		return  self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'pos.itemID')
	          ->with('mats', 'functions')
	          ->where('mapDenormalize.solarSystemID', '=', $id)
	          ->count();
	}

	public static function getCountOfMappedMoonsInRegionByID($id)
	{
		return  self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'pos.itemID')
	          ->with('mats', 'functions')
	          ->where('mapDenormalize.regionID', '=', $id)
	          ->count();
	}

	public static function getRecentUpdates($paginate = 5)
	{
		return self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'pos.itemID')
	       ->join('mglinski_eve_sde.mapRegions', 'mapRegions.regionID', '=', 'mapDenormalize.regionID')
	       ->join('mglinski_eve_sde.mapSolarSystems', 'mapSolarSystems.solarSystemID', '=', 'mapDenormalize.solarSystemID')
	       ->with('mats', 'functions')
	       ->orderBy('pos.updated_at', 'desc')
	       ->paginate($paginate);
	}

	public static function getRecentUpdatesBySolarSystemID($systemID, $paginate = 5)
	{
		return self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'pos.itemID')
			->join('mglinski_eve_sde.mapRegions', 'mapRegions.regionID', '=', 'mapDenormalize.regionID')
			->join('mglinski_eve_sde.mapSolarSystems', 'mapSolarSystems.solarSystemID', '=', 'mapDenormalize.solarSystemID')
			->with('mats', 'functions')->orderBy('pos.updated_at', 'desc')
			->where('mapDenormalize.solarSystemID', '=', $systemID)
			->paginate($paginate);
	}

	public static function getRecentUpdatesByRegionID($regionID, $paginate = 5)
	{
		return self::join('mglinski_eve_sde.mapDenormalize', 'mapDenormalize.itemID', '=', 'pos.itemID')
	       ->join('mglinski_eve_sde.mapRegions', 'mapRegions.regionID', '=', 'mapDenormalize.regionID')
			->join('mglinski_eve_sde.mapSolarSystems', 'mapSolarSystems.solarSystemID', '=', 'mapDenormalize.solarSystemID')
	       ->with('mats', 'functions')->orderBy('pos.updated_at', 'desc')
	       ->where('mapDenormalize.regionID', '=', $regionID)
	       ->paginate($paginate);
	}
}
<?php

class RegionItem extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'mapRegions';

	/**
	 * The database connection used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'eve_data';

	/**
	 * The database column primary key.
	 *
	 * @var string
	 */
	protected $primaryKey = 'regionID';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array();

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	public static $joveRegions = array(10000004, 10000017, 10000019);

}
<?php

class Station extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'station';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	public static $planetGroupID = 7;
	public static $groupID = 15;

	public static $types = array(
		'54' => 'Caldari Logistics Station',
		'56' => 'Gallente Military Station',
		'57' => 'Gallente Station Hub ',
		'1529' => 'Caldari Administrative Station',
		'1530' => 'Caldari Research Station',
		'1531' => 'Caldari Trading Station',
		'1926' => 'Amarr Station Hub',
		'1927' => 'Amarr Station Military',
		'1928' => 'Amarr Industrial Station',
		'1929' => 'Amarr Standard Station',
		'1930' => 'Amarr Mining Station',
		'1931' => 'Amarr Research Station',
		'1932' => 'Amarr Trade Post',
		'2071' => 'Station (Interbus)',
		'2496' => 'Minmatar Hub',
		'2497' => 'Minmatar Industrial Station',
		'2498' => 'Minmatar Military Station',
		'2499' => 'Minmatar Mining Station',
		'2500' => 'Minmatar Research Station',
		'2501' => 'Minmatar Station',
		'2502' => 'Minmatar Trade Post',
		'3864' => 'Amarr Citadel',
		'3865' => 'Gallente Research Station',
		'3866' => 'Gallente Trading Hub',
		'3867' => 'Gallente Industrial Station',
		'3868' => 'Gallente Administrative Station',
		'3869' => 'Gallente Logistics Station',
		'3870' => 'Gallente Mining Station',
		'3871' => 'Caldari Station Hub',
		'3872' => 'Caldari Military Station',
		'4023' => 'Caldari Mining Station',
		'4024' => 'Caldari Food Processing Plant Station',
		'9856' => 'Amarr Class A Starport',
		'9857' => 'Amarr Class B Starport',
		'9866' => 'Angel Serpentis',
		'9867' => 'Heaven',
		'9868' => 'Concord Starbase',
		'9873' => 'Dark Amarr Station O',
		'9874' => 'Dark Amarr Station H',
		'10795' => 'Jovian Construct',
		'12242' => 'Station (Conquerable 1)',
		'12294' => 'Station (Conquerable 2)',
		'12295' => 'Station (Conquerable 3)',
		'19757' => 'Research Outpost',
		'21642' => 'Caldari Research Outpost',
		'21644' => 'Amarr Factory Outpost',
		'21645' => 'Gallente Administrative Outpost',
		'21646' => 'Minmatar Service Outpost',
		'22296' => 'Blood Raider Logistics Outpost',
		'22297' => 'Blood Raider Military Outpost',
		'22298' => 'Blood Raider Trading Outpost',
	);

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array(
		'id',
		'itemID',
		'name',
		'type',
		'solarSystemID',
		'corporationID',
		'corporationName',
		'timer'
	);

	public static function getBySystemID($id)
	{
		$return = self::where('station.solarSystemID', '=', $id)->get();

		return $return;
	}

	public static function getByID($id)
	{
		$return = self::where('station.id', '=', $id)->get();

		if(isset($return[0]))
		{
			return $return[0];
		}
		return false;
	}

}
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// GUEST REQUIRED ROUTES
Route::group(array('before' => 'guest'), function()
{
	Route::get('login', array('as' => 'login', 'uses' => 'LoginController@loginView'));
	Route::post('login', array('uses' => 'LoginController@loginAction'));
});

// LOGIN REQUIRED ROUTES
Route::group(array('before' => 'auth'), function()
{
	// Basic URLs
	Route::get('/', array('as' => 'home', 'uses' => 'CelestialController@universeView'));
	Route::get('search', array('as' => 'search', 'uses' => 'CelestialController@universeSearchMethod'));

	Route::get('search', array('as' => 'search', 'uses' => 'SearchController@universeSearchMethod'));
	Route::get('search/celestials.json', array('as' => 'search_celestial', 'uses' => 'SearchController@searchCelestialAction'));
	Route::get('search/corps.json', array('as' => 'search_corp', 'uses' => 'SearchController@searchCorpsAction'));

	Route::group(array('prefix' => 'import'), function()
	{
		Route::get('anna', array('as' => 'import_anna', 'uses' => 'ImportController@importAnnaView'));
		Route::post('anna', array('uses' => 'ImportController@importAnnaAction'));

		Route::get('anna/confirm', array('as' => 'import_anna_confirm', 'uses' => 'ImportController@importAnnaConfirmView'));
		Route::post('anna/confirm', array('uses' => 'ImportController@importAnnaConfirmAction'));

		// ----------------------------------------

		Route::get('catch', array('as' => 'import_catch', 'uses' => 'ImportController@importCatchView'));
		Route::post('catch', array('uses' => 'ImportController@importCatchAction'));

		Route::get('catch/confirm', array('as' => 'import_catch_confirm', 'uses' => 'ImportController@importCatchConfirmView'));
		Route::post('catch/confirm', array('uses' => 'ImportController@importCatchConfirmAction'));


		// ----------------------------------------

		Route::get('tenerifis', array('as' => 'import_tenerifis', 'uses' => 'ImportController@importTenerifisView'));
		Route::post('tenerifis', array('uses' => 'ImportController@importTenerifisAction'));

		// ----------------------------------------

		Route::get('recon', array('as' => 'import_recon', 'uses' => 'ImportController@importReconView'));
		Route::post('recon', array('uses' => 'ImportController@importReconAction'));

		Route::get('recon/confirm', array('as' => 'import_recon_confirm', 'uses' => 'ImportController@importReconConfirmView'));
		Route::post('recon/confirm', array('uses' => 'ImportController@importReconConfirmAction'));

		// ----------------------------------------

		Route::get('falcons', array('as' => 'import_falcons', 'uses' => 'ImportController@importFalconsView'));
		Route::post('falcons', array('uses' => 'ImportController@importFalconsAction'));

		Route::get('falcons/confirm', array('as' => 'import_falcons_confirm', 'uses' => 'ImportController@importFalconsConfirmView'));
		Route::post('falcons/confirm', array('uses' => 'ImportController@importFalconsConfirmAction'));
	});

	// Map Listings
	Route::group(array('prefix' => 'map'), function()
	{
		Route::get('region/{id}/export', array('as' => 'region_export', 'uses' => 'CelestialController@universeRegionExport'));
		Route::get('region/{id}/{system}', array('as' => 'region_system', 'uses' => 'CelestialController@universeRegionView'));
		Route::get('region/{id}', array('as' => 'region', 'uses' => 'CelestialController@universeRegionView'));
		Route::get('system/{id}/export', array('as' => 'system_export', 'uses' => 'CelestialController@universeSystemExport'));
		Route::get('system/{id}', array('as' => 'system', 'uses' => 'CelestialController@universeSystemView'));
	});

	// POSs
	Route::group(array('prefix' => 'pos'), function()
	{
		Route::get('add/{id}', array('as' => 'pos_add', 'uses' => 'PosController@insertView'));
		Route::get('edit/{id}', array('as' => 'pos_edit', 'uses' => 'PosController@editView'));

		Route::group(array('before' => 'edit'), function()
		{
			Route::post('add/{id}', array('uses' => 'PosController@insertAction'));
			Route::post('edit/{id}', array('uses' => 'PosController@editAction'));
			Route::get('delete/{id}', array('as' => 'pos_delete', 'uses' => 'PosController@deleteAction'));
		});
	});

	// POCOs
	Route::group(array('prefix' => 'poco'), function()
	{
		Route::get('add/{id}', array('as' => 'poco_add', 'uses' => 'PocoController@insertView'));
		Route::get('edit/{id}', array('as' => 'poco_edit', 'uses' => 'PocoController@editView'));

		Route::group(array('before' => 'edit'), function()
		{
			Route::post('add/{id}', array('uses' => 'PocoController@insertAction'));
			Route::post('edit/{id}', array('uses' => 'PocoController@editAction'));
			Route::get('delete/{id}', array('as' => 'poco_delete', 'uses' => 'PocoController@deleteAction'));
		});
	});

	// iHubs
	Route::group(array('prefix' => 'ihub'), function()
	{
		Route::get('add/{id}', array('as' => 'ihub_add', 'uses' => 'IhubController@insertView'));
		Route::get('edit/{id}', array('as' => 'ihub_edit', 'uses' => 'IhubController@editView'));

		Route::group(array('before' => 'edit'), function()
		{
			Route::post('add/{id}', array('uses' => 'IhubController@insertAction'));
			Route::post('edit/{id}', array('uses' => 'IhubController@editAction'));
			Route::get('delete/{id}', array('as' => 'ihub_delete', 'uses' => 'IhubController@deleteAction'));
		});
	});

	// Stations
	Route::group(array('prefix' => 'station'), function()
	{
		//Route::get('add/{id}', array('as' => 'station_add', 'uses' => 'StationController@insertView'));
		Route::get('edit/{id}', array('as' => 'station_edit', 'uses' => 'StationController@editView'));

		Route::group(array('before' => 'edit'), function()
		{
			//Route::post('add/{id}', array('uses' => 'StationController@insertAction'));
			Route::post('edit/{id}', array('uses' => 'StationController@editAction'));
			//Route::get('delete/{id}', array('as' => 'station_delete', 'uses' => 'StationController@deleteAction'));
		});
	});
		
	//
	Route::get('logout', array('as' => 'logout', 'uses' => 'LoginController@logoutAction'));
});

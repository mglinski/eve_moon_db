<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

// General app commands
Artisan::add(new SetupCommand);
Artisan::add(new NewUserCommand);

// Import commands
Artisan::add(new MassUpdateCommand);
Artisan::add(new DotlanMapsImportCommand);
Artisan::add(new SovereigntyImportCommand);
Artisan::add(new OutpostImportCommand);
Artisan::add(new AlliancesImportCommand);

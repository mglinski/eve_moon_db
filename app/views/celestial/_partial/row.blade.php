<tr>
    <td <?php if($celestial->groupID != \Poco::$groupID) { ?>style="padding-left:30px"<?php } ?>>
        <label title="<?=$celestial->itemID?>" class="label <?= ($celestial->groupID == \Poco::$groupID ? 'label-primary' : 'label-default') ?>"><?= ($celestial->groupID == \Poco::$groupID ? 'P' : 'M') ?></label>
        <?=$name?>
        <?php
        if(isset($structure->status) and $celestial->groupID == \Pos::$groupID)
        {
            if($structure->type == 0)
            {
                ?>
                <label class="pull-right label label-danger" style="margin-bottom: 0;line-height: 13px;padding-top: 3px;margin-top: 2px;">No POS</label>
                <?php
            }
            else
            {
                ?>
                <label class="pull-right label <?=($structure->status == 0 ? 'label-danger' : 'label-success')?>" style="margin-bottom: 0;line-height: 13px;padding-top: 3px;margin-top: 2px;">
                    <?=\Pos::$towerStatusTypes[$structure->status]?>
                </label>
                <?php
            }
        }

        if (isset($structure->owner) and ctype_digit($structure->owner)) {
            // show advanced names

            $corp = Corporation::getCorp($structure->owner);
            $structure->owner = $corp->corporationShortName.' ['.$corp->allianceShortName.']';
        }

        if($celestial->groupID == \Poco::$groupID)
        {
            ?>
            <label class="pull-right label label-default" style="margin-bottom: 0;line-height: 13px;padding-top: 3px;margin-top: 2px;">
                <?=\Poco::$planetTypes[$celestial->typeID]?>
            </label>
            <?php
        }
        ?>
    </td>
    <td>
        <?php
        if($celestial->groupID == \Poco::$groupID)
        {
            if(isset($structure->owner))
            {
                echo $structure->owner;
            }
            else
            {
                ?>
                <label style="background-color: #666" class="label label-default">No Data</label>
                <?php
            }
        }
        else
        {
            if (isset($structure->owner))
            {
                echo $structure->owner;
            }
        }
        ?>
    </td>
    <td>
        <?php
        if($structure != false)
        {
            if($celestial->groupID == \Poco::$groupID)
            {
                echo $structure->tax . '% Tax';
            }
            else
            {
                $x = 0;
                foreach($structure->functions as $function)
                {
                    if ($structure->type == 0) {
                        continue;
                    }

                    if ($x % 2 == 0 and $x != 0) {
                        echo "<br />";
                    }
                    $x++;
                    ?><label class="label label-default"><?=Functions::$functionTypes[$function->functionID]?></label> <?php
                }
            }
        }
        ?>
    </td>
    <td>
        <?php
        if($structure != false)
        {
            if ($celestial->groupID == \Pos::$groupID) {
                echo Mat::getHTMLFromList($structure->mats);
            }
        }
        else
        {
            if($celestial->groupID == \Pos::$groupID)
            {
                ?>
                <label style="background-color: #111" class="label label-default">No Data</label>
                <?php
            }
        }
        ?>
    </td>
    <td>
        <div class="pull-right">
            <?php
            if($structure != false)
            {
                if($celestial->groupID == \Poco::$groupID)
                {
                    ?>
                    <a href="<?= URL::route('poco_edit', array($celestial->itemID)) ?>" class="btn btn-info btn-xs">Edit</a>
                    <a href="<?= URL::route('poco_delete', array($celestial->itemID)) ?>" class="btn btn-danger btn-xs deleteButton">Delete</a>
                    <?php
                }
                else
                {
                    ?>
                    <a href="<?= URL::route('pos_edit', array($celestial->itemID)) ?>" class="btn btn-info btn-xs">Edit</a>
                    <a href="<?= URL::route('pos_delete', array($celestial->itemID)) ?>" class="btn btn-danger btn-xs deleteButton">Delete</a>
                    <?php
                }
            }
            else
            {
                if($celestial->groupID == \Poco::$groupID)
                {
                    ?>
                    <a href="<?= URL::route('poco_add', array($celestial->itemID)) ?>" class="btn btn-success btn-xs">Add</a>
                    <?php
                }
                else
                {
                    ?>
                    <a href="<?= URL::route('pos_add', array($celestial->itemID)) ?>" class="btn btn-success btn-xs">Add</a>
                    <?php
                }
            }
            ?>
        </div>
    </td>
</tr>
<script>
    $(document).ready(function(){
        $("#corporationID").select2({
            placeholder: "Search for Corporations",
            minimumInputLength: 3,
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                url: "<?=URL::route('search_corp')?>",
                dataType: 'jsonp',
                data: function(term, page) {
                    return {
                        q:          term, // search term
                        page_limit: 10
                    };
                },
                results: function(data, page){ // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    return {results: data.locations};
                }
            },
            initSelection: function(element, callback)
            {
                <?php
                if($loc !== false)
                {
                    $name = $loc->corporationName.' ['.$loc->corporationShortName.']';
                    if ($loc->allianceID != 0)
    					$name .= ' ('.$loc->allianceName.' ['.$loc->allianceShortName.'])';
                    ?>
                    callback({id: <?=$loc->corporationID?>, text: <?=json_encode($name)?> });
                    <?php
                }
            ?>
            },
            formatResult:  function(location){
                return "<span>" + location.text + "</span>";
            }, // omitted for brevity, see the source of this page
            formatSelection:  function(location){
                return location.text;
            },  // omitted for brevity, see the source of this page
            dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
            escapeMarkup:     function(m){
                return m;
            } // we do not want to escape markup since we are displaying html in results
        });
    });
</script>
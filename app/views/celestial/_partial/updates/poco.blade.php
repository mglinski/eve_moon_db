
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h4>
			Pocos
		</h4>
	</div>
</div>
<hr style="margin-top:0" />
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<table class="table table-hover table-striped table-bordered table-condensed">
			<thead>
			<tr>
				<th>Planet Name</th>
				<th>Region</th>
				<th>Owner</th>
				<th>Type</th>
				<th>Tax Rate</th>
				<th>Date</th>
				<th>Actions</th>
			</tr>
			</thead>
			<tbody>

			@foreach($pocoUpdates as $row)
				@include('celestial._partial.updates.table.pocoRow', ['row' => $row])
			@endforeach

			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h4>
			POS Towers
		</h4>
	</div>
</div>

<hr style="margin-top:0" />

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<table class="table table-hover table-striped table-bordered table-condensed">
			<thead>
			<tr>
				<th>Moon Name</th>
				<th>Region</th>
				<th>Tower</th>
				<th>Online?</th>
				<th>Owner</th>
				<th>Date</th>
				<th>Functions</th>
				<th>Mats</th>
				<th>Actions</th>
			</tr>
			</thead>
			<tbody>
			@foreach($posUpdates as $row)
				@include('celestial._partial.updates.table.posRow', ['row' => $row])
			@endforeach
			</tbody>
		</table>
	</div>
</div>
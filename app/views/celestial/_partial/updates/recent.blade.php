<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3>
            Recent Updates
        </h3>
    </div>
</div>

@include('celestial._partial.updates.pos', ['posUpdates' => $posUpdates])

@include('celestial._partial.updates.poco', ['pocoUpdates' => $pocoUpdates])
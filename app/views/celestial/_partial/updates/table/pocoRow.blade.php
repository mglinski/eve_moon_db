<?php
if (isset($row->owner) and ctype_digit($row->owner)) {
	// show advanced names

	$corp = Corporation::getCorp($row->owner);
	$row->owner = $corp->corporationShortName.' ['.$corp->allianceShortName.']';
}
?>
<tr>
	<td>
		<a href="<?=URL::route('system', array($row->solarSystemName))?>"><?=$row->itemName?></a>
	</td>
	<td><?=$row->regionName?></td>
	<td><?=$row->owner?></td>
	<td><label class="label label-default"><?=Poco::$planetTypes[$row->typeID]?></label></td>
	<td><?=$row->tax?>%</td>
	<td><?=$row->updated_at?></td>
	<td>
		<a href="<?=URL::route('poco_edit', array($row->itemID))?>" class="btn btn-info btn-xs">Edit</a>
		<a href="<?=URL::route('poco_delete', array($row->itemID))?>" class="btn btn-danger btn-xs deleteButton">Delete</a>
	</td>
</tr>
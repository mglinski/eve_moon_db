<?php
if (isset($row->owner) and ctype_digit($row->owner)) {
	// show advanced names

	$corp = Corporation::getCorp($row->owner);
	$row->owner = $corp->corporationShortName.' ['.$corp->allianceShortName.']';
}
?>

<tr>
	<td>
		<a href="<?=URL::route('system', array($row->solarSystemName))?>"><?=$row->itemName?></a>
	</td>
	<td><?=$row->regionName?></td>
	<td><?=Pos::$towerTypes[$row->type]?></td>
	<td>
		<?php
		if ($row->type == 0) {
			?>
			<label class="label label-danger">No POS</label>
		<?php
		}
		else {
			?>
			<label class="label <?=($row->status == 0 ? 'label-danger' : 'label-success')?>">
				<?=Pos::$towerStatusTypes[$row->status]?>
			</label>
		<?php
		}
		?>
	</td>
	<td><?=$row->owner?></td>
	<td><?=$row->updated_at?></td>
	<td>
		<?php
		$x = 0;
		foreach ($row->functions as $function) {
			if ($row->type == 0) {
				continue;
			}

			if ($x % 2 == 0 and $x != 0) {
				echo "<br />";
			}
			$x++;
			?>
			<label class="label label-default"><?=Functions::$functionTypes[$function->functionID]?></label> <?php
		}
		?>
	</td>
	<td>
		<?=Mat::getHTMLFromList($row->mats)?>
	</td>
	<td>
		<a href="<?=URL::route('pos_edit', array($row->itemID))?>" class="btn btn-info btn-xs">Edit</a>
		<a href="<?=URL::route('pos_delete', array($row->itemID))?>" class="btn btn-danger btn-xs deleteButton">Delete</a>
	</td>
</tr>

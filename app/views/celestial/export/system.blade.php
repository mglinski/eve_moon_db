<?php
$return = [
    'celestials' => []
];

foreach($celestials as $celestial)
{
    $structure = false;
    if ($celestial->groupID == \Poco::$groupID) {
        $structure = \Poco::getByID($celestial->itemID);
    }
    else {
        $structure = \Pos::getByID($celestial->itemID);
    }

    if (!isset($structure->itemID)) {
        continue;
    }

    if (isset($structure->owner) and ctype_digit($structure->owner)) {
        // show advanced names
        $corp = \Corporation::getCorp($structure->owner);
        $structure->owner = $corp->corporationShortName.' ['.$corp->allianceShortName.']';
    }

    // Export data
    $data = [
        'celestial_id' => $celestial->itemID,
        'celestial_name' => $celestial->itemName,
        'celestial_type' => ($celestial->groupID == \Pos::$groupID ? "Moon" : "Planet"),
        'celestial_type_id' => $celestial->groupID,
        'structure_present' => (isset($structure->itemID)),
        'structure_status' => (isset($structure->status) and $celestial->groupID == \Pos::$groupID) ? \Pos::$towerStatusTypes[$structure->status] : 'Online',
        'structure_status_id' => (isset($structure->status) and $celestial->groupID == \Pos::$groupID) ? $structure->status : '1',
        'structure_type' => isset($structure->type) ? \Pos::$towerTypes[$structure->type] : 'Customs Office',
        'structure_type_id' => isset($structure->type) ? $structure->type : '2233',
        'structure_owner' => isset($structure->owner) ? $structure->owner : 'No Data',
    ];

    // get all pos functions
    if ($celestial->groupID !== \Poco::$groupID and isset($structure->status)) {

        $function_array = [];
        foreach($structure->functions as $function)
        {
            if ($structure->type == 0) {
                continue;
            }
            $function_array[] = \Functions::$functionTypes[$function->functionID];
        }

        // get all moon materials
        $mat_array = [];
        foreach($structure->mats as $mat)
        {
            if ($structure->type == 0) {
                continue;
            }

            $mat_array[] = [
                'typeID' => $mat->matID,
                'name' => \Mat::$matTypes[$mat->matID]
            ];
        }

        $data['structure_password'] = isset($structure->password) ? $structure->password : '';
        $data['structure_notes'] = isset($structure->notes) ? $structure->notes : '';
        $data['structure_dscan'] = isset($structure->dscan) ? $structure->dscan : '';
        $data['structure_functions'] = $function_array;
        $data['celestial_materials'] = $mat_array;
    }

    // collect data
    $return['celestials'][] = $data;
}

echo json_encode($return);
<div class="page-header" style="margin: 0 0 20px;">
	<h2>
		Home
	</h2>
</div>

<ol class="breadcrumb">
	<li class="active">Home</li>
</ol>

<?php
if(Session::has('flash_msg'))
{
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="flash_msg" class="alert alert-info"><?= Session::get('flash_msg') ?></div>
		</div>
	</div>
<?php
}
?>

<?php
if(Session::has('flash_error'))
{
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="flash_msg" class="alert alert-info"><?= Session::get('flash_error') ?></div>
		</div>
	</div>
<?php
}
?>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Regions List</h3>

		<div class="row">
			<?php
			foreach ($regions as $i => $region)
			{
			if ($i % 4 == 0)
			{
			?></div>
		<div class="row"><?php
			}
			?>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<a href="<?= URL::route('region', array(urlencode($region->regionName))) ?>" class="btn btn-sm btn-block <?= (($region->factionID != null and $region->factionID != 500014) ? 'btn-default highsec' : 'btn-default nullsec') ?>" style="">
					<?= $region->regionName ?>
				</a>
			</div>
			<?php
			}
			?>
		</div>
	</div>
</div>

<hr/>

@include('celestial._partial.updates.recent', ['posUpdates' => $posUpdates, 'pocoUpdates' => $pocoUpdates])

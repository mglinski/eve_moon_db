<div class="page-header" style="margin: 0 0 20px;">
	<h2>
		<a href="<?=URL::route('system', array($ihub->solarSystemID))?>" class="pull-right btn btn-default">Back to System</a>
		Edit iHub: <strong><?=$ihub->itemName?></strong>
	</h2>
</div>

<?php

if (Session::has('flash_error'))
{
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="flash_error" class="alert alert-danger"><?=Session::get('flash_error')?></div>
		</div>
	</div>
	<?php
}
?>

<?=Form::open(array('route' => array('ihub_edit', $ihub->itemID)))?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h4>iHub Details</h4>
			<div class="form-group">
				<?=Form::hidden('itemID', $ihub->itemID, array('id' => 'itemID'))?>
				<h4><label class="label label-default"><?=$solarSystem->solarSystemName?></label></h4>
			</div>

			<div class="form-group">
				<?=Form::label('planet', 'Planet')?>
				<?=Form::select('planet', $planets, $ihub->itemID, array('id' => 'status', 'class' => 'form-control'))?>
			</div>

			<div class="form-group">
				<?=Form::label('status', 'Status')?>
				<?=Form::select('status', Ihub::$statusTypes, $ihub->status, array('id' => 'status', 'class' => 'form-control'))?>
			</div>

			<div class="form-group">
				<?=Form::label('owner', 'Owner')?>
				<?=Form::text('owner', $ihub->owner, array('id' => 'type', 'class' => 'form-control'))?>
			</div>

			<div class="form-group">
				<?=Form::label('corporationID', 'POS Corp')?>
				<?=Form::text('corporationID', (ctype_digit($ihub->owner) ? $ihub->owner : ''), array('id' => 'corporationID', 'class' => 'form-control'))?>
			</div>

			<div class="form-group">
				<?=Form::label('timer', 'Timer')?>
				<div class="input-group">
					<?=Form::text('timer', $ihub->timer, array('id' => 'timer', 'class' => 'form-control'))?>
					<span class="input-group-addon">Time</span>
				</div>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</div>
	</div>
</form>

@include('celestial._partial.select2', ['loc' => Corporation::getCorp( (ctype_digit($ihub->owner) ? $ihub->owner : false) )])
<div class="page-header" style="margin: 0 0 20px;">
	<h2>
		<a href="<?=URL::route('system', array($celestial->solarSystemID))?>" class="pull-right btn btn-default">Back to System</a>
		Add POCO: <strong><?=$celestial->itemName?></strong>
	</h2>
</div>

<?php
if (Session::has('flash_error'))
{
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="flash_error" class="alert alert-danger"><?=Session::get('flash_error')?></div>
		</div>
	</div>
	<?php
}
?>

<?=Form::open(array('route' => array('poco_add', $celestial->itemID)))?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h4>POCO Details</h4>
			<div class="form-group">
				<?=Form::hidden('itemID', $celestial->itemID, array('id' => 'itemID'))?>
				<h4><label class="label label-default"><?=$celestial->itemName?></label></h4>
			</div>

			<div class="form-group">
				<?=Form::label('owner', 'Owner')?>
				<?=Form::text('owner', '', array('id' => 'type', 'class' => 'form-control'))?>
			</div>

			<div class="form-group">
				<?=Form::label('corporationID', 'POS Corp')?>
				<?=Form::text('corporationID', '', array('id' => 'corporationID', 'class' => 'form-control'))?>
			</div>

			<div class="form-group">
				<?=Form::label('tax', 'Tax Rate')?>
				<div class="input-group">
					<?=Form::text('tax', '', array('id' => 'tax', 'class' => 'form-control'))?>
					<span class="input-group-addon">%</span>
				</div>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Add POS</button>
			</div>
		</div>
	</div>
</form>


@include('celestial._partial.select2', ['loc' => false ])
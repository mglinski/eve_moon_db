<div class="page-header" style="margin: 0 0 20px;">
	<h2>
		<a href="<?=URL::route('system', array($poco->solarSystemID))?>" class="pull-right btn btn-default">Back to System</a>
		Edit POCO: <strong><?=$poco->itemName?></strong>
	</h2>
</div>

<?php

if (Session::has('flash_error'))
{
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="flash_error" class="alert alert-danger"><?=Session::get('flash_error')?></div>
		</div>
	</div>
	<?php
}
?>

<?=Form::open(array('route' => array('poco_edit', $poco->itemID)))?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h4>POCO Details</h4>
			<div class="form-group">
				<?=Form::hidden('itemID', $poco->itemID, array('id' => 'itemID'))?>
				<h4><label class="label label-default"><?=$poco->itemName?></label></h4>
			</div>

			<div class="form-group">
				<?=Form::label('owner', 'Owner')?>
				<?=Form::text('owner', $poco->owner, array('id' => 'type', 'class' => 'form-control'))?>
			</div>

			<div class="form-group">
				<?=Form::label('corporationID', 'POS Corp')?>
				<?=Form::text('corporationID', (ctype_digit($poco->owner) ? $poco->owner : ''), array('id' => 'corporationID', 'class' => 'form-control'))?>
			</div>

			<div class="form-group">
				<?=Form::label('tax', 'Tax Rate')?>
				<div class="input-group">
					<?=Form::text('tax', $poco->tax, array('id' => 'tax', 'class' => 'form-control'))?>
					<span class="input-group-addon">%</span>
				</div>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</div>
	</div>
</form>

@include('celestial._partial.select2', ['loc' => Corporation::getCorp( (ctype_digit($poco->owner) ? $poco->owner : false) )])
<div class="page-header" style="margin: 0 0 20px;">
	<h2>
		<a href="<?=URL::route('system', array($celestial->solarSystemID))?>" class="pull-right btn btn-default">Back to System</a>
		Add POS: <strong><?=$celestial->itemName?></strong>
	</h2>
</div>

<?php
if (Session::has('flash_error')) {
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="flash_error" class="alert alert-danger"><?=Session::get('flash_error')?></div>
		</div>
	</div>
<?php
}
?>

<?=Form::open(array('route' => array('pos_add', $celestial->itemID)))?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h4>POS Details</h4>

		<div class="form-group">
			<?=Form::hidden('itemID', $celestial->itemID, array('id' => 'itemID'))?>
			<h4><label class="label label-default"><?=$celestial->itemName?></label></h4>
		</div>

		<div class="form-group">
			<?=Form::label('type', 'POS Type')?>
			<?=Form::select('type', Pos::$towerTypes, array(), array('id' => 'type', 'class' => 'form-control'))?>
		</div>

		<div class="form-group">
			<?=Form::label('status', 'Is POS Online?')?>
			<?=Form::select('status', Pos::$towerStatusTypes, array(), array('id' => 'status', 'class' => 'form-control'))?>
		</div>

		<div class="form-group">
			<?=Form::label('owner', 'POS Owner')?>
			<?=Form::text('owner', '', array('id' => 'owner', 'placeholder' => 'Enter Owner Name', 'class' => 'form-control'))?>
		</div>

		<div class="form-group">
			<?=Form::label('corporationID', 'POS Corp')?>
			<?=Form::text('corporationID', '', array('id' => 'corporationID', 'class' => 'form-control'))?>
		</div>

		<div class="form-group">
			<?=Form::label('password', 'POS Password')?>
			<?=Form::text('password', '', array('id' => 'password', 'placeholder' => 'Enter Password', 'class' => 'form-control'))?>
		</div>

		<div class="form-group">
			<?=Form::label('dscan', 'D-Scan')?>
			<?=Form::textarea('dscan', '', array('id' => 'dscan', 'rows' => '5', 'placeholder' => 'Copy+Paste Short DScan Here', 'class' => 'form-control'))?>
		</div>

		<div class="form-group">
			<?=Form::label('notes', 'POS Notes')?>
			<?=Form::textarea('notes', '', array('id' => 'notes', 'rows' => '5', 'placeholder' => 'Notes on POS', 'class' => 'form-control'))?>
		</div>

	</div>
</div>

<hr />

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h4>
			<a href="#" class="pull-right btn btn-sm btn-info addMaterial" style="margin-top: -10px;">Add Material</a>
			Moon Materials
		</h4>

		<div class="matsContainer">
			<?php
			if (!Input::old('mat')) {
				?>
				<div class="form-group">
					<?=Form::select('mat[]', Mat::$matTypes, '', array('id' => 'mats', 'class' => 'form-control'))?>
					<?=Form::text('mat_abundance[]', '1', array('placeholder' => 'Enter Mineral Abundance', 'class' => 'form-control'))?>
				</div>
				<div class="form-group">
					<div class="input-group">
						<select name="mat[]" class="form-control">
							<?php
							foreach (Mat::$matTypes as $x => $name) {
								?>
								<option value="<?=$x?>"><?=$name?></option>
							<?php
							}
							?>
						</select>
						<?=Form::text('mat_abundance[]', '1', array('placeholder' => 'Enter Mineral Abundance', 'class' => 'form-control'))?>
						<span class="input-group-btn" style="vertical-align: top;">
					        <a class="btn btn-danger deleteMat" href="#" type="button">Remove</a>
					    </span>
					</div>
				</div>
			<?php
			}
			else {
				if (is_array(Input::old('mat'))) {
					$data = Input::old('mat');
					foreach ($data as $i => $matID) {
						?>
						<div class="form-group">
							<div class="input-group">
								<select name="mat[]" class="form-control">
									<?php
									foreach (Mat::$matTypes as $x => $name) {
										?>
										<option value="<?=$x?>" <?=($matID == $x ? 'selected="selected"' : '')?>><?=$name?></option>
									<?php
									}
									?>
								</select>
								<?=Form::text('mat_abundance[]', '1', array('placeholder' => 'Enter Mineral Abundance', 'class' => 'form-control'))?>
								<span class="input-group-btn" style="vertical-align: top;">
						        <a class="btn btn-danger deleteMat" href="#" type="button">Remove</a>
						    </span>
							</div>
						</div>
					<?php
					}
				}
				else {
					?>
					<div class="form-group">
						<?=Form::select('mat[]', Mat::$matTypes, '', array('id' => 'mats', 'class' => 'form-control'))?>
					</div>
				<?php
				}
			}
			?>
		</div>

	</div>
</div>

<hr />

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h4>
			<a href="#" class="pull-right btn btn-sm btn-info addRole" style="margin-top: -10px;">Add Role</a>
			POS Functions
		</h4>

		<div class="functionsContainer">
			<?php
			if (!Input::old('function')) {
				?>
				<div class="form-group">
					<div class="input-group">
						<select name="function[]" class="form-control">
							<?php
							foreach (Functions::$functionTypes as $x => $name) {
								?>
								<option value="<?=$x?>"><?=$name?></option>
							<?php
							}
							?>
						</select>
						<span class="input-group-btn">
					        <a class="btn btn-danger deleteFunction" href="#" type="button">Remove</a>
					    </span>
					</div>
				</div>
			<?php
			}
			else {
				if (is_array(Input::old('function'))) {
					$data = Input::old('function');
					foreach ($data as $i => $functionID) {
						// dont show delete button on first item
						if ($i === 0) {
							?>
							<div class="form-group">
								<div class="input-group">
									<select name="function[]" class="form-control">
										<?php
										foreach (Functions::$functionTypes as $x => $name) {
											?>
											<option value="<?=$x?>" <?=($functionID == $x ? 'selected="selected"' : '')?>><?=$name?></option>
										<?php
										}
										?>
									</select>
								<span class="input-group-btn">
							        <a class="btn btn-danger deleteFunction" href="#" type="button">Remove</a>
							    </span>
								</div>
							</div>
						<?php
						}
						else {
							?>
							<div class="form-group">
								<div class="input-group">
									<select name="function[]" class="form-control">
										<?php
										foreach (Functions::$functionTypes as $x => $name) {
											?>
											<option value="<?=$x?>" <?=($functionID == $x ? 'selected="selected"' : '')?>><?=$name?></option>
										<?php
										}
										?>
									</select>

								<span class="input-group-btn">
							        <a class="btn btn-danger deleteFunction" href="#" type="button">Remove</a>
							    </span>
								</div>
							</div>
						<?php
						}
					}
				}
				else {
					?>
					<div class="form-group">
						<?=Form::select('function[]', Functions::$functionTypes, '', array('id' => 'functions', 'class' => 'form-control'))?>
					</div>
				<?php
				}
			}
			?>
		</div>

		<button type="submit" class="btn btn-primary">Add POS</button>
	</div>
</div>
</form>

<script>
	$(document).ready(function(){
		$('.addMaterial').on('click', function(){
			if($('.matsContainer .form-group').length > 4){
				alert('5 Minerals Max');
				return false;
			}

			var template = $('.matTemplate').clone().removeClass('matTemplate').show();
			$('.matsContainer').append(template);
			return false;
		});

		$('.addRole').on('click', function(){
			if($('.functionContainer .form-group').length > 5){
				alert('6 Roles Max');
				return false;
			}

			var template = $('.functionTemplate').clone().removeClass('functionTemplate').show();
			$('.functionsContainer').append(template);
			return false;
		});

		$(document).on('click', '.deleteMat', function(){
			$(this).parent().parent().parent().remove();
			//alert('working');
			return false;
		});

		$(document).on('click', '.deleteFunction', function(){
			$(this).parent().parent().parent().remove();
			//alert('working');
			return false;
		});

		$(document).on('change', '#type', function(){
			if($(this).val() == '0'){
				$('#status').val(0).prop('disabled', true).after('<input id="status_hidden" type="hidden" name="status" value="0" />');
				$('#owner').val('').prop('readOnly', true);
				$('#password').val('').prop('readOnly', true);
			}
			else{
				$('#status').prop('disabled', false);
				$('#status_hidden').remove();
				$('#owner').prop('readOnly', false);
				$('#password').prop('readOnly', false);
			}
			return false;
		});

		$('#type').change();

	});
</script>

@include('celestial._partial.select2', ['loc' => false])

<!-- JS Templates -->
<div class="form-group matTemplate" style="display:none">
	<div class="input-group">
		<select class="form-control" name="mat[]">
			<?php
			foreach (Mat::$matTypes as $id => $name) {
				?>
				<option value="<?=$id?>"><?=$name?></option><?php
			}
			?>
		</select>
		<input placeholder="Enter Mineral Abundance" class="form-control" name="mat_abundance[]" type="text" value="1" />
		<span class="input-group-btn" style="vertical-align: top;">
	        <a class="btn btn-danger deleteMat" href="#" type="button">Remove</a>
	    </span>
	</div>
</div>
<div class="form-group functionTemplate" style="display:none">
	<div class="input-group">
		<select class="form-control" name="function[]">
			<?php
			foreach (Functions::$functionTypes as $id => $name) {
				?>
				<option value="<?=$id?>"><?=$name?></option><?php
			}
			?>
		</select>
		<span class="input-group-btn">
	        <a class="btn btn-danger deleteFunction" href="#" type="button">Remove</a>
	    </span>
	</div>
</div>
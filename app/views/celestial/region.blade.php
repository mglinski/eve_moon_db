<div class="page-header" style="margin: 0 0 20px;">
	<h2>
		<a href="<?=URL::route('home')?>" class="pull-right btn btn-default">Back to All</a>
		Region: <?=$region->regionName?>
	</h2>
</div>

<ol class="breadcrumb">
	<li><a href="<?=URL::route('home')?>">Home</a></li>
	<li class="active"><?=$region->regionName?></li>
</ol>

<?php
if (Session::has('flash_msg'))
{
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="flash_msg" class="alert alert-info"><?=Session::get('flash_msg')?></div>
		</div>
	</div>
<?php
}

if (Session::has('flash_error'))
{
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="flash_msg" class="alert alert-info"><?=Session::get('flash_error')?></div>
		</div>
	</div>
<?php
}
?>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Region Systems</h3>
		<div class="row">
			<?php
			foreach($systems as $i => $system)
			{
				//$master_count = MapItem::where('solarSystemID', '=', $system->solarSystemID)->whereIn('groupID', array(7,8))->count();
				$master_count = MapItem::where('solarSystemID', '=', $system->solarSystemID)->where('groupID', '=', 8)->count();
				$pos_count = Pos::getCountOfMappedMoonsInSystemByID($system->solarSystemID);
				//$poco_count = Poco::getCountOfMappedPlanetsInSystemByID($system->solarSystemID);
				//$item_count = $pos_count + $poco_count;
				$item_count = $pos_count;

				if($i % 6 == 0)
				{
					?></div><div class="row"><?php
				}
				?>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
					<a href="<?=URL::route('system', array($system->solarSystemName))?>" class="btn btn-sm btn-block <?=(($system->security > 0.5) ? 'btn-default highsec' : 'btn-default nullsec')?>" style="">
						<?php

						/*if(rand(0,10) == 10)
						{
							*/?><!--<span class="glyphicon glyphicon-exclamation-sign" style="color:#3875d7" title="System Needs Update"></span> &nbsp;--><?php
/*						}*/

						echo $system->solarSystemName;

						if($item_count == $master_count)
						{
							?>&nbsp; <span class="glyphicon glyphicon-ok" style="color:#398439" title="System Fully Probed"></span><?php
						}
						else
						{
							?>&nbsp; <span class="glyphicon glyphicon-remove" style="color: #D11F19" title="System Not Fully Probed (<?=$item_count?> of <?=$master_count?>)"></span><?php
						}

						if($item_count == 0)
						{
							?>&nbsp; <span class="glyphicon glyphicon-ban-circle" style="color:#FF0000" title="No Data for this System"></span> <?php
						}
						?>
					</a>
				</div>
				<?php
			}
			?>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>
			Region Stats
		</h3>
	</div>
</div>
<hr style="margin-top:0" />

<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
		<dl>
			<dt>Number of Moons</dt>
			<dd><?=$pos_num?></dd>

			<dt>Number of Planets</dt>
			<dd><?=$poco_num?></dd>
		</dl>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
		<dl>
			<dt>Number of Moons Mapped</dt>
			<dd><?=$pos_mapped_num?></dd>

			<dt>Number of Planets Mapped</dt>
			<dd><?=$poco_mapped_num?></dd>
		</dl>
	</div>
</div>

<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
		<?php
		$i = 0;
		$x = 0;
		$listcount = array();
		foreach($mats as $name => $count)
		{
			if($i % 4 == 0 and $i != 0)
			{
				$x++;
			}
			if(!isset($listcount[$x]))
			{
				$listcount[$x] = 0;
			}
			$listcount[$x] += $count;
			$i++;
		}
		?>
		<div>
			<strong>R8 (<?=$listcount[0]?>)</strong>
		</div>
		<?php
		$list = array('R16', 'R32', 'R64');
		$i = 0;
		$r = 2;
		$list_c = 0;
		$mat_names = array_flip(Mat::$matTypes);
		$mat_names = array_flip(Mat::$matTypes);
		foreach($mats as $name => $count)
		{
			if($i % 4 == 0 and $i != 0)
			{
				?>
				</div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<div><strong><?=$list[$list_c++]?>  (<?=$listcount[($r - 1)]?>)</strong></div>
				<?php
				$r++;
			}
			?>
			<div>
				<label class="label label-default" style="background-color:<?=Mat::$matRarityColor[(string)$r]?>"><a style="color: #fff;" href="<?=URL::route('search')?>?region=<?=$region->regionID?>&material=<?=$mat_names[$name]?>"><?=ucwords($name)?></a></label> <?=$count?>
			</div>
			<?php
			$i++;
		}
		?>
	</div>
</div>


<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>
			<a href="http://evemaps.dotlan.net/map/<?=str_replace(' ', '_', $region->regionName)?>" target="_blank" class="pull-right btn btn-info btn-sm">Dotlan Region Link</a>
			Region Map
		</h3>
	</div>
</div>
<hr style="margin-top:0" />

<object id="map" data="<?=URL::to('maps/'.str_replace(' ', '_', $region->regionName))?>.svg" type="image/svg+xml" width="1024" height="768"></object>


@include('celestial._partial.updates.recent', ['posUpdates' => $posUpdates, 'pocoUpdates' => $pocoUpdates])

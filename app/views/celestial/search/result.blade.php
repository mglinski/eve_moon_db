<div class="page-header" style="margin: 0 0 20px;">
	<h2>
		Search Results
	</h2>
</div>

<ol class="breadcrumb">
	<li><a href="<?=URL::route('home')?>">Home</a></li>
	<li class="active">Search Results</li>
</ol>

<?php
if(Session::has('flash_msg'))
{
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="flash_msg" class="alert alert-info"><?= Session::get('flash_msg') ?></div>
		</div>
	</div>
<?php
}
?>

<?php
if(Session::has('flash_error'))
{
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="flash_msg" class="alert alert-info"><?= Session::get('flash_error') ?></div>
		</div>
	</div>
<?php
}
?>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>
			<div class="btn-group pull-right">
				<button type="button" class="btn btn-default btn-sm moonCrapSwitch">Moon Crap</button>
			</div>
			Celestial List
		</h3>
		<div>
			<table class="table table-hover table-striped table-bordered table-condensed">
				<thead>
				<tr>
					<th style="width:36%">Name</th>
					<th>Owner</th>
					<th>Flags</th>
					<th>Minerals</th>
				</tr>
				</thead>
				<tbody>
				<?php
				foreach($celestials as $celestial)
				{
					$name = $celestial->itemName;
					$structure = false;
					if($celestial->groupID == Poco::$groupID)
					{
						$structure = Poco::getByID($celestial->itemID);
						$name = $celestial->itemName;
					}
					else
					{
						$structure = Pos::getByID($celestial->itemID);
						$name = $celestial->itemName;
					}

					if (isset($structure->owner) and ctype_digit($structure->owner)) {
						// show advanced names

						$corp = Corporation::getCorp($structure->owner);
						$structure->owner = $corp->corporationShortName.' ['.$corp->allianceShortName.']';
					}
					?>
					<tr>
						<td <?php if($celestial->groupID != Poco::$groupID) { ?>style="padding-left:30px"<?php } ?>>
							<label title="<?=$celestial->itemID?>" class="label <?= ($celestial->groupID == Poco::$groupID ? 'label-primary' : 'label-default') ?>"><?= ($celestial->groupID == Poco::$groupID ? 'P' : 'M') ?></label>
							<?php

							$matches = [];
							$split = preg_match('/(.+) (.+) - (Moon [0-9]+)/', $name, $matches);

							if($structure != false)
							{
								if($celestial->groupID == \Poco::$groupID)
								{
									?>
									<a href="<?= URL::route('system', array(urlencode($celestial->solarSystemID))) ?>"><?=$matches[1]?></a>
									<a href="<?= URL::route('poco_edit', array($celestial->itemID)) ?>"><?=$matches[2]?> - <?=$matches[3]?></a>
									<?php
								}
								else
								{
									?>
									<a href="<?= URL::route('system', array(urlencode($celestial->solarSystemID))) ?>"><?=$matches[1]?></a>
									<a href="<?= URL::route('pos_edit', array($celestial->itemID)) ?>"><?=$matches[2]?> - <?=$matches[3]?></a>
									<?php
								}
							}
							else
							{
								if($celestial->groupID == \Poco::$groupID)
								{
									?>
									<a href="<?= URL::route('system', array(urlencode($celestial->solarSystemID))) ?>"><?=$matches[1]?></a>
									<a href="<?= URL::route('poco_add', array($celestial->itemID)) ?>"><?=$matches[2]?> - <?=$matches[3]?></a>
									<?php
								}
								else
								{
									?>
									<a href="<?= URL::route('system', array(urlencode($celestial->solarSystemID))) ?>"><?=$matches[1]?></a>
									<a href="<?= URL::route('pos_add', array($celestial->itemID)) ?>"><?=$matches[2]?> - <?=$matches[3]?></a>
									<?php
								}
							}

							if(isset($structure->status) and $celestial->groupID == Pos::$groupID)
							{
								if($structure->type == 0)
								{
									?>
									<label class="pull-right label label-danger" style="margin-bottom: 0;line-height: 13px;padding-top: 3px;margin-top: 2px;">No POS</label>
								<?php
								}
								else
								{
									?>
									<label class="pull-right label <?=($structure->status == 0 ? 'label-danger' : 'label-success')?>"  style="margin-bottom: 0;line-height: 13px;padding-top: 3px;margin-top: 2px;">
										<?=Pos::$towerStatusTypes[$structure->status]?>
									</label>
								<?php
								}

							}

							if($celestial->groupID == Poco::$groupID)
							{
								?>
								<label class="pull-right label label-default"  style="margin-bottom: 0;line-height: 13px;padding-top: 3px;margin-top: 2px;">
									<?=Poco::$planetTypes[$celestial->typeID]?>
								</label>
							<?php
							}
							?>
						</td>
						<td>
							<?php
							if($celestial->groupID == Poco::$groupID)
							{
								if(isset($structure->owner))
								{
									echo $structure->owner;
								}
								else
								{
									?>
									<label style="background-color: #666" class="label label-default">No Data</label>
								<?php
								}
							}
							else
							{
								if(isset($structure->owner))
								{
									echo $structure->owner;
								}
							}
							?>
						</td>
						<td>
							<?php
							if($structure != false)
							{
								if($celestial->groupID == Poco::$groupID)
								{
									echo $structure->tax.'% Tax';
								}
								else
								{
									$x = 0;
									foreach($structure->functions as $function)
									{
										if($structure->type == 0)
										{
											continue;
										}

										if($x % 2 == 0 and $x != 0)
										{
											echo "<br />";
										}
										$x++;
										?><label class="label label-default"><?=Functions::$functionTypes[$function->functionID]?></label> <?php
									}
								}
							}
							?>
						</td>
						<td>
							<?php
							if($structure != false)
							{
								if($celestial->groupID == Pos::$groupID)
								{
									echo Mat::getHTMLFromList($structure->mats);
								}
							}
							else
							{
								if($celestial->groupID == Pos::$groupID)
								{
									?>
									<label style="background-color: #111" class="label label-default">No Data</label>
								<?php
								}
							}
							?>
						</td>
					</tr>
				<?php
				}
				?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<hr />
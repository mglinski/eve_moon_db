<div class="page-header" style="margin: 0 0 20px;">
	<h2>
		Search
	</h2>
</div>

<ol class="breadcrumb">
	<li><a href="<?=URL::route('home')?>">Home</a></li>
	<li class="active">Search</li>
</ol>

<?php
if(Session::has('flash_msg'))
{
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="flash_msg" class="alert alert-info"><?= Session::get('flash_msg') ?></div>
		</div>
	</div>
<?php
}
?>

<?php
if(Session::has('flash_error'))
{
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="flash_msg" class="alert alert-info"><?= Session::get('flash_error') ?></div>
		</div>
	</div>
<?php
}
?>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php
		foreach($regions as $reg)
		{
			$regions_list[$reg->regionID] = $reg->regionName;
		}
		?>

		<?=Form::open(array('route' => 'search', 'method' => 'get'))?>
			<div class="form-group">
				<?=Form::label('region', 'Region')?>
				<?=Form::select('region', $regions_list, '', array('id' => 'region', 'placeholder' => 'Copy + Paste Here', 'class' => 'form-control'))?>
			</div>

			<div class="form-group">
				<?=Form::label('material', 'Moon Material')?>
				<?=Form::select('material', $materials, '', array('id' => 'material', 'placeholder' => 'Copy + Paste Here', 'class' => 'form-control'))?>
			</div>

			<div class="form-group">
				<input class="btn btn-success" type="submit" value="Find Moons" />
			</div>
		</form>

	</div>
</div>

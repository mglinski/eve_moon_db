<div class="page-header" style="margin: 0 0 20px;">
	<h2>
		<a href="<?=URL::route('system', array($station->solarSystemID))?>" class="pull-right btn btn-default">Back to System</a>
		Edit Station: <strong><?=$station->itemName?></strong>
	</h2>
</div>

<?php

if (Session::has('flash_error'))
{
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="flash_error" class="alert alert-danger"><?=Session::get('flash_error')?></div>
		</div>
	</div>
	<?php
}
?>

<?=Form::open(array('route' => array('station_edit', $station->id)))?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h4>Station Details</h4>
			<div class="form-group">
				<?=Form::hidden('itemID', $station->itemID, array('id' => 'itemID'))?>
				<h4><label class="label label-default"><?=$solarSystem->solarSystemName?></label></h4>
			</div>

			<div class="form-group">
				<?=Form::label('timer', 'Timer')?>
				<div class="input-group">
					<?=Form::text('timer', $station->timer, array('id' => 'timer', 'class' => 'form-control'))?>
					<span class="input-group-addon">Time</span>
				</div>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</div>
	</div>
</form>
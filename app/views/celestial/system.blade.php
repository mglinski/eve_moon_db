<div class="page-header" style="margin: 0 0 20px;">
	<h2>
		<a href="<?=URL::route('region', array(urlencode($region->regionName)))?>" class="pull-right btn btn-default">Back to Region</a>
		System: <?= $system->solarSystemName ?>
	</h2>
</div>

<ol class="breadcrumb">
	<li><a href="<?=URL::route('home')?>">Home</a></li>
	<li><a href="<?=URL::route('region', array($region->regionName))?>"><?=$region->regionName?></a></li>
	<li class="active"><?=$system->solarSystemName?></li>
</ol>

<?php
if(Session::has('flash_msg'))
{
?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div id="flash_msg" class="alert alert-info"><?= Session::get('flash_msg') ?></div>
	</div>
</div>
<?php
}

if(Session::has('flash_error'))
{
?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div id="flash_msg" class="alert alert-info"><?= Session::get('flash_error') ?></div>
	</div>
</div>
<?php
}
?>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div>

			<h3>Sov Owner</h3>

			<?php
			$sov = Sovereignty::find($system->solarSystemID);
			if ($sov->allianceID == 0 and $sov->factionID == 0) {
				// UNCLAIMED OMG
				$faction = Faction::find($sov->factionID);

				?>
				<div class="alert alert-danger"><strong><i class="glyphicon glyphicon-warning-sign"></i> NO SOV NO SOV</strong><br /><br /> ༼ つ ◕_◕ ༽つ  GIVE SOV</div>
				<?php
			}
			else if ($sov->factionID != 0) {
				// NPC System
				$faction = Faction::find($sov->factionID);
				?>
				<div class="media">
					<a class="media-left" href="#">
						<img src="https://image.eveonline.com/Alliance/<?=$faction->factionID?>_64.png" alt="<?=$faction->factionName?>">
					</a>
					<div class="media-body">
						<h4 class="media-heading">NPC Faction</h4>
						<a href="#"><?=$faction->factionName?></a>
					</div>
				</div>
				<?php
			}
			else {
				// Player System
				$corp = Corporation::find($sov->corporationID);
				$alliance = Alliance::find($sov->allianceID);
				?>
				<div class="media">
					<a class="media-left" href="#">
						<img src="https://image.eveonline.com/Alliance/<?=$alliance->id?>_64.png" alt="<?=$alliance->name?>">
					</a>
					<div class="media-body">
						<h4 class="media-heading">Alliance</h4>
						<a href="#"><?=$alliance->name?></a>
					</div>
				</div>
				<div class="media">
					<a class="media-left" href="#">
						<img src="https://image.eveonline.com/Corporation/<?=$corp->id?>_64.png" alt="<?=$corp->name?>">
					</a>
					<div class="media-body">
						<h4 class="media-heading">Corporation</h4>
						<a href="#"><?=$corp->name?></a>
					</div>
				</div>
				<?php
			}
			?>

			<hr />

			<h3>Stations</h3>
			<table class="table table-hover table-striped table-bordered table-condensed">
				<thead>
				<tr>
					<th style="width:36%">Name</th>
					<th>Owner</th>
					<th>Timer</th>
					<th>Type</th>
					<th style="width:15%">Actions</th>
				</tr>
				</thead>
				<tbody>
				<?php
				foreach($stations as $station)
				{
				$celestial = MapItem::find($station->itemID);
				$corp = Corporation::getCorp($station->corporationID);
				$station_owner = $corp->corporationShortName.' ['.$corp->allianceShortName.']';
				?>
				<tr>
					<td><?=$station->name?></td>
					<td><?=$station_owner?></td>
					<td><?=$station->timer?></td>
					<td><?=Station::$types[$station->type]?></td>
					<td>
						<a href="<?= URL::route('station_edit', array($station->id)) ?>" class="btn btn-info btn-xs">Edit</a>
					</td>
				</tr>
				<?php
				}
				?>
				</tbody>
			</table>

			<hr />

			<h3>
				<div class="btn-group pull-right">
					<a href="<?= URL::route('ihub_add', array($system->solarSystemID)) ?>" class="btn btn-success btn-sm">Add iHub</a>
				</div>
				iHubs
			</h3>
			<table class="table table-hover table-striped table-bordered table-condensed">
				<thead>
				<tr>
					<th style="width:36%">Name</th>
					<th>Owner</th>
					<th>Timer</th>
					<th>Status</th>
					<th style="width:15%">Actions</th>
				</tr>
				</thead>
				<tbody>
				<?php
				foreach($ihubs as $ihub)
				{
					$celestial = MapItem::find($ihub->itemID);
					if (isset($ihub->owner) and ctype_digit($ihub->owner)) {
						// show advanced names

						$corp = Corporation::getCorp($ihub->owner);
						$ihub->owner = $corp->corporationShortName.' ['.$corp->allianceShortName.']';
					}
					?>
					<tr>
						<td><?=$celestial->itemName?></td>
						<td><?=$ihub->owner?></td>
						<td><?=$ihub->timer?></td>
						<td><?=Ihub::$statusTypes[$ihub->status]?></td>
						<td>
							<a href="<?= URL::route('ihub_edit', array($ihub->itemID)) ?>" class="btn btn-info btn-xs">Edit</a>
							<a href="<?= URL::route('ihub_delete', array($ihub->itemID)) ?>" class="btn btn-danger btn-xs deleteButton">Delete</a>
						</td>
					</tr>
					<?php
				}
				?>
				</tbody>
			</table>

			<hr />

			<h3>
				<div class="btn-group pull-right">
					<button type="button" class="btn btn-default btn-sm moonCrapSwitch">Moon Crap</button>
				</div>
				Celestial List
			</h3>
			<table class="table table-hover table-striped table-bordered table-condensed">
				<thead>
				<tr>
					<th style="width:36%">Name</th>
					<th>Owner</th>
					<th>Flags</th>
					<th>Minerals</th>
					<th style="width:15%">Actions</th>
				</tr>
				</thead>
				<tbody>
				<?php
				foreach($celestials as $celestial)
				{
					$structure = false;
					if ($celestial->groupID == Poco::$groupID) {
						$structure = Poco::getByID($celestial->itemID);
						$name = $celestial->itemName;
					}
					else {
						$structure = Pos::getByID($celestial->itemID);
						$name = explode("-", $celestial->itemName);
						$name = $name[count($name) - 1];
					}

                    if (isset($structure->owner) and ctype_digit($structure->owner)) {
                        // show advanced names

                        $corp = Corporation::getCorp($structure->owner);
                        $structure->owner = $corp->corporationShortName.' ['.$corp->allianceShortName.']';
                    }
					?>
					<tr>
						<td <?php if($celestial->groupID != \Poco::$groupID) { ?>style="padding-left:30px"<?php } ?>>
							<label title="<?=$celestial->itemID?>" class="label <?= ($celestial->groupID == \Poco::$groupID ? 'label-primary' : 'label-default') ?>"><?= ($celestial->groupID == \Poco::$groupID ? 'P' : 'M') ?></label>
							<?=$name?>
							<?php
							if(isset($structure->status) and $celestial->groupID == \Pos::$groupID)
							{
                                if($structure->type == 0)
                                {
                                    ?>
                                    <label class="pull-right label label-danger" style="margin-bottom: 0;line-height: 13px;padding-top: 3px;margin-top: 2px;">No POS</label>
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <label class="pull-right label <?=($structure->status == 0 ? 'label-danger' : 'label-success')?>" style="margin-bottom: 0;line-height: 13px;padding-top: 3px;margin-top: 2px;">
                                        <?=\Pos::$towerStatusTypes[$structure->status]?>
                                    </label>
                                    <?php
                                }
							}

							if($celestial->groupID == \Poco::$groupID)
							{
                                ?>
                                <label class="pull-right label label-default" style="margin-bottom: 0;line-height: 13px;padding-top: 3px;margin-top: 2px;">
                                    <?=\Poco::$planetTypes[$celestial->typeID]?>
                                </label>
                                <?php
							}
							?>
						</td>
						<td>
							<?php
							if($celestial->groupID == \Poco::$groupID)
							{
                                if(isset($structure->owner))
                                {
                                    echo $structure->owner;
                                }
                                else
                                {
                                    ?>
                                    <label style="background-color: #666" class="label label-default">No Data</label>
                                    <?php
                                }
							}
							else
							{
								if (isset($structure->owner))
								{
									echo $structure->owner;
								}
							}
							?>
						</td>
						<td>
							<?php
							if($structure != false)
							{
                                if($celestial->groupID == \Poco::$groupID)
                                {
                                    echo $structure->tax . '% Tax';
                                }
                                else
                                {
                                    $x = 0;
                                    foreach($structure->functions as $function)
                                    {
                                        if ($structure->type == 0) {
                                            continue;
                                        }

                                        if ($x % 2 == 0 and $x != 0) {
                                            echo "<br />";
                                        }
                                        $x++;
                                        ?><label class="label label-default"><?=Functions::$functionTypes[$function->functionID]?></label> <?php
                                    }
                                }
							}
							?>
						</td>
						<td>
							<?php
							if($structure != false)
							{
								if ($celestial->groupID == \Pos::$groupID) {
									echo Mat::getHTMLFromList($structure->mats);
								}
							}
							else
							{
                                if($celestial->groupID == \Pos::$groupID)
                                {
                                    ?>
                                    <label style="background-color: #111" class="label label-default">No Data</label>
                                    <?php
                                }
							}
							?>
						</td>
						<td>
							<div class="pull-right">
								<?php
								if($structure != false)
								{
                                    if($celestial->groupID == \Poco::$groupID)
                                    {
                                        ?>
                                        <a href="<?= URL::route('poco_edit', array($celestial->itemID)) ?>" class="btn btn-info btn-xs">Edit</a>
                                        <a href="<?= URL::route('poco_delete', array($celestial->itemID)) ?>" class="btn btn-danger btn-xs deleteButton">Delete</a>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <a href="<?= URL::route('pos_edit', array($celestial->itemID)) ?>" class="btn btn-info btn-xs">Edit</a>
                                        <a href="<?= URL::route('pos_delete', array($celestial->itemID)) ?>" class="btn btn-danger btn-xs deleteButton">Delete</a>
                                        <?php
                                    }
								}
								else
								{
                                    if($celestial->groupID == \Poco::$groupID)
                                    {
                                        ?>
                                        <a href="<?= URL::route('poco_add', array($celestial->itemID)) ?>" class="btn btn-success btn-xs">Add</a>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <a href="<?= URL::route('pos_add', array($celestial->itemID)) ?>" class="btn btn-success btn-xs">Add</a>
                                        <?php
                                    }
								}
								?>
							</div>
						</td>
					</tr>
					<?php
				}
				?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<hr />

@include('celestial._partial.updates.recent', ['posUpdates' => $posUpdates, 'pocoUpdates' => $pocoUpdates])
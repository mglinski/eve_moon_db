<div class="page-header" style="margin: 0 0 20px;">
	<h2>
		Mass Import: Falcons
	</h2>
</div>

<?php
if (Session::has('flash_error'))
{
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="flash_error" class="alert alert-danger"><?=Session::get('flash_error')?></div>
		</div>
	</div>
<?php
}

if (Session::has('flash_msg'))
{
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="flash_error" class="alert alert-info"><?=Session::get('flash_msg')?></div>
		</div>
	</div>
<?php
}
?>

<?=Form::open(array('route' => 'import_falcons'))?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=Form::label('massImport', 'Mass Import Copy + Paste')?>
			<?=Form::textarea('massImport', '', array('id' => 'massImport', 'placeholder' => 'Copy + Paste Here', 'class' => 'form-control'))?>
		</div>

		<div class="form-group">
			<input class="btn btn-success" type="submit" value="Import Data" />
		</div>
	</div>
</div>
</form>

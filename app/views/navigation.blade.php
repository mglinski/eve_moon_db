<!-- Static navbar -->
<div class="navbar navbar-default navbar-static-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?=URL::route('home')?>">Brave Newbies, Inc.</a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li <?php if(Request::is('home')){?> class="active"<?php }?>>
					<a href="<?=URL::route('home')?>">Home</a>
					<?php
					if(!Auth::guest())
					{
						?>
						</li>
						<li <?php if(Request::is('search')){?> class="active"<?php }?>>
							<a href="<?=URL::route('search')?>">Search</a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Import Bulk Data <b class="caret"></b></a>
							<li <?php if(Request::is('import_tenerifis')){?> class="active"<?php }?>>
								<a href="<?=URL::route('import_tenerifis')?>">Mass Import - Tenerifis</a>
							</li>

							<ul class="dropdown-menu">
								<!--
								<li <?php if(Request::is('import_catch')){?> class="active"<?php }?>>
									<a href="<?=URL::route('import_catch')?>">Mass Import - Catch</a>
								</li>
								<li <?php if(Request::is('import_falcons')){?> class="active"<?php }?>>
									<a href="<?=URL::route('import_falcons')?>">Mass Import for Falcons</a>
								</li>
								<li <?php if(Request::is('import_anna')){?> class="active"<?php }?>>
									<a href="<?=URL::route('import_anna')?>">Mass Import for Anna</a>
								</li>
								-->
								<li <?php if(Request::is('import_recon')){?> class="active"<?php }?>>
									<a href="<?=URL::route('import_recon')?>">Mass Import for Recon</a>
								</li>
							</ul>
						</li>
						<?php
					}
					?>
			</ul>
			<ul class="nav navbar-nav pull-right">
				<?php
				if(!Auth::guest())
				{
					?>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, <strong><?=Auth::user()->username?></strong> <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="<?=URL::route('logout')?>">Logout</a></li>
						</ul>
					</li>
					<?php
				}
				else
				{
					?>
					<li <?php if(Request::is('login')){?> class="active"<?php }?>><a href="<?=URL::route('login')?>">Login</a></li>
					<?php
				}
				?>
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</div>
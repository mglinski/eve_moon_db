## Eve Moon Database

Eve Moon Database Is a tool for tracking and storing information about Eve Online Player Owned Structures. You can track who owns a POS, what it is equiped with, passwords, any moon minerals that are on the moon that its anchored under, etc.

### License

Eve Moon Database is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
